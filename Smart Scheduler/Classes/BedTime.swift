import UIKit
class BedTime: Codable {
    
    var id: Int
    var startTime: Date
    var endTime: Date
   
    init(id: Int, startTime: Date, endTime: Date){
        self.id = id
        self.startTime = startTime
        self.endTime = endTime
       
        
    }
}
