class Classes: Codable {
    
    var id: Int
    var name: String?
    var time: String?
    var priority: Int?
    var notes: String?
    
    init(id: Int, name: String?, time: String?, priority: Int?, notes: String?){
        self.id = id
        self.name = name
        self.time = time
        self.priority = priority
        self.notes = notes
        
    }
}
