import UIKit
struct ExtraCur: Codable {
    
    var id: Int
    var name: String?
    var startTime: Date
    var endTime: Date
    
    init(id: Int, name: String?, startTime: Date,  endTime: Date){
        self.id = id
        self.name = name
        self.startTime = startTime
        self.endTime = endTime
        
        
    }
    /*init(id: Int, name: String?, startTime: String?, endTime: String?){
        self.id = id
        self.name = name
        self.startTime = startTime
        self.endTime = endTime*/
}
