struct userCal: Codable {
    var userCalTitle: String
    init(userCalTitle: String) {
        self.userCalTitle = userCalTitle
    }
}

struct userNotifications: Codable {
    var enabled: Bool
    init(enabled: Bool) {
        self.enabled = enabled
    }
}
struct saveICloudSync: Codable {
    var enabled: Bool
    init(enabled: Bool) {
          self.enabled = enabled
      }
}

struct saveDynScheduling: Codable {
    var enabled: Bool
    init(enabled: Bool) {
          self.enabled = enabled
      }
}
