import UIKit
class RelaxSetTime: Codable {
    var startTime: Date
    var endTime: Date
    var name: String
    init(startTime: Date, endTime: Date, name: String) {
        self.startTime = startTime
        self.endTime = endTime
        self.name = name
    }

}
