import UIKit
class RelaxTime: Codable {
    var time: Int
    var sessions: String
    
    init(time: Int, sessions: String) {
        self.time = time
        self.sessions = sessions
    }
    
}
