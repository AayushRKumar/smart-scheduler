struct StudyHallClass {
    var name: String
    var length: Int
    init(name: String, length: Int) {
        self.name = name
        self.length = length
    }
}
