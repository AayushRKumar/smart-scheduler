//
//  AddClassesVC.swift
//  Smart Scheduler
//
//  Created by Ankit R Kumar on 8/5/18.
//  Copyright © 2018 CompuBaba. All rights reserved.
//

import UIKit
 var classesList = [Classes]()

class AddClassesVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    var notesDone = false
    var db: OpaquePointer?
    var currentNotes = ""
    
    @IBOutlet weak var classesTable: UITableView!
    @IBOutlet weak var classNameTextField: UITextField!
   
    @IBOutlet var enterWeak: UIButton!
    
    @IBOutlet var classTimeTextField: UITextField!
    @IBOutlet weak var priorityTextField: UITextField!
    
    @IBAction func clear(_ sender: Any) {
        classNameTextField.text=""
        classTimeTextField.text=""
        
        priorityTextField.text=""
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
  
    override func viewWillAppear(_ animated: Bool)
        
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTotalView"), object: nil)

        if #available(iOS 13.0, *) {
         
            if traitCollection.userInterfaceStyle == .dark {
             
                view.backgroundColor = UIColor.black
           
           
            } else {
                view.backgroundColor = UIColor.white
            
            }
        } else {
            view.backgroundColor = UIColor.white
        }
    }
    
    @IBAction func done(_ sender: Any) {
        doneAction()
        
    }
    func ac(title: String, message: String, button: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
        present(alert, animated: true)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ClassesTableViewCell
        let classes: Classes
        
        classes = classesList[indexPath.row]
        cell.timeLabel.text = "\(classes.time ?? "0") minutes"
        cell.classLabel.text = classes.name
        cell.priortyLabel.text = "\(classes.priority ?? 5)"
        return cell
    }
    func doneAction() {
        let name = classNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let time = classTimeTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let priority = Int((priorityTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)
        var makeReturn = false
        if(name?.isEmpty)!{
            classNameTextField.layer.borderColor = UIColor.red.cgColor
            
            makeReturn = true
        }
        
        if(time?.isEmpty)!{
            classTimeTextField.layer.borderColor = UIColor.red.cgColor
            
            makeReturn = true
        }
        
        if(priorityTextField.text == ""){
            priorityTextField.layer.borderColor = UIColor.red.cgColor
            
            makeReturn = true
        }
        
        
        
        if makeReturn == true {
            makeReturn = false
            ac(title: "Invalid", message: "Please fill all boxes", button: "OK")
            return
        }
        
        if priority! < 1 || priority! > 10 {
            ac(title: "Invalid", message: "One or more fields is invalid", button: "OK")
            priorityTextField.layer.borderColor = UIColor.red.cgColor
            return
            
        }
        
        if Int(time!)! > 800 {
            ac(title: "Too Much TIme", message: "", button: "OK")
            classTimeTextField.layer.borderColor = UIColor.red.cgColor
            return
        }
        
         if inDarkMode {
                   classNameTextField.layer.borderColor = UIColor.white.cgColor
                                classTimeTextField.layer.borderColor = UIColor.white.cgColor
                               priorityTextField.layer.borderColor = UIColor.white.cgColor
               } else {
               classNameTextField.layer.borderColor = UIColor.black.cgColor
                     classTimeTextField.layer.borderColor = UIColor.black.cgColor
                    priorityTextField.layer.borderColor = UIColor.black.cgColor
               }
        
        
        classNameTextField.text=""
        classTimeTextField.text=""
        
        priorityTextField.text=""
      notesDone = false
                    
        
        let ac = UIAlertController(title: "Notes", message: "Enter Notes About the homework here", preferredStyle: .alert)
      
            ac.addTextField()
        let submit = UIAlertAction(title: "Enter", style: .default) {
            [weak self, weak ac] action in
                           
                guard let answer = ac?.textFields?[0].text else { return }
            
            self?.currentNotes = answer
           
            self?.notesDone = true
            classesList.append(Classes(id: 0, name: name, time: time, priority: priority, notes: self?.currentNotes))
            self?.save()
                 print("Data saved successfully")
                       }
                     
            ac.addAction(submit)
            self.present(ac, animated: true)
        
          
       
     

    }
    func save() {
      
        let jsonEncoder = JSONEncoder()
        if let saveData = try? jsonEncoder.encode(classesList) {
            let defaults = UserDefaults.standard
            defaults.set(saveData, forKey: "SYNChomework")
        } else {
            print("Failed TO load")
        }
        
        
         self.classesTable.reloadData()
      ///  let queryString = "SELECT * FROM ClassesTable"
        
      //  var stmt:OpaquePointer?
        
//        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
//            let errmsg = String(cString: sqlite3_errmsg(db)!)
//            print("error preparing insert: \(errmsg)")
//            return
//        }
        
//        while(sqlite3_step(stmt) == SQLITE_ROW){
//            let id = sqlite3_column_int(stmt, 0)
//            let name = String(cString: sqlite3_column_text(stmt, 1))
//            let startTime = String(cString: sqlite3_column_text(stmt, 2))
//            let endTime = String(cString: sqlite3_column_text(stmt, 3))
//            let priority = String(cString: sqlite3_column_text(stmt, 4))
           //   }
//            classesList.append(Classes(id: Int(id), name: String(describing: name), startTime: String(describing: startTime), endTime: String(describing: endTime), priority: String(describing: priority)))
        

        
       
    }
    
    @IBOutlet var clearWeak: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
          classNameTextField.delegate = self
        if inDarkMode {
            classNameTextField.layer.borderColor = UIColor.white.cgColor
                         classTimeTextField.layer.borderColor = UIColor.white.cgColor
                        priorityTextField.layer.borderColor = UIColor.white.cgColor
        } else {
        classNameTextField.layer.borderColor = UIColor.black.cgColor
              classTimeTextField.layer.borderColor = UIColor.black.cgColor
             priorityTextField.layer.borderColor = UIColor.black.cgColor
        }
        classNameTextField.layer.cornerRadius = 10
        classNameTextField.clipsToBounds = true
        classNameTextField.layer.borderWidth = 1
        
      
        classTimeTextField.layer.cornerRadius = 10
        classTimeTextField.clipsToBounds = true
        classTimeTextField.layer.borderWidth = 1
        
       
        priorityTextField.layer.cornerRadius = 10
        priorityTextField.clipsToBounds = true
        priorityTextField.layer.borderWidth = 1
        clearWeak.titleLabel?.adjustsFontSizeToFitWidth = true
        classTimeTextField.adjustsFontSizeToFitWidth = true
        let defaults = UserDefaults.standard
        if let savedClass = defaults.object(forKey: "SYNChomework") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                classesList = try jsonDecoder.decode([Classes].self, from: savedClass)
                print(classesList)
                
            } catch {
                print("Failed TO load")
            }
        }
        
       

//        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//            .appendingPathComponent("ClassesDataBase.sqlite")
        
        
//        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
//            print("error opening database")
//        }
//
//        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS ClassesTable (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, startTime TEXT, endTime TEXT, priority TEXT)", nil, nil, nil) != SQLITE_OK {
//            let errmsg = String(cString: sqlite3_errmsg(db)!)
//            print("error creating table: \(errmsg)")
//        }
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last! as String
        print(documentPath)

        
    }
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
       
        let edit = UIContextualAction(style: .normal, title: "Edit") { _,_,_ in
                  let ac = UIAlertController(title: "Edit", message: "Choose what you would like to edit", preferredStyle: .actionSheet)
             ac.popoverPresentationController?.sourceView = self.classesTable as UIView
                      ac.popoverPresentationController?.sourceRect = self.classesTable.rectForRow(at: indexPath)
                   let editName = UIAlertAction(title: "Edit Name", style: .default) {
                       [weak self] action in
                       let newAC = UIAlertController(title: "New Name", message: nil, preferredStyle: .alert)
                       newAC.addTextField()
                       let submit = UIAlertAction(title: "Enter", style: .default) {
                           [weak self, weak newAC] action in
                           
                           guard let answer = newAC?.textFields?[0].text else { return }
                        let test = answer.trimmingCharacters(in: .whitespacesAndNewlines)
                        if test.isEmpty {
                                self?.classesTable.reloadData()
                                                return }
                        
                           self?.submitNewClass(answer, path: indexPath.row)
                           
                       }
                       newAC.addAction(submit)
                       self?.present(newAC, animated: true)
                       
                   }
                   ac.addAction(editName)
                   let editTime = UIAlertAction(title: "Edit Time", style: .default) {
                       [weak self] action in
                       let newAC = UIAlertController(title: "New Time", message: nil, preferredStyle: .alert)
                       newAC.addTextField(configurationHandler: { textField in
                           textField.keyboardType = .numberPad
                       })
                       let submit = UIAlertAction(title: "Enter", style: .default) {
                           [weak self, weak newAC] action in
                           guard let answer = newAC?.textFields?[0].text else { return }
                        let test = answer.trimmingCharacters(in: .whitespacesAndNewlines)
                            if test.isEmpty {
                                            self?.classesTable.reloadData()
                                return }
                           self?.submitNewTime(answer, path: indexPath.row)
                       }
                       newAC.addAction(submit)
                       self?.present(newAC, animated: true)
                        
                   }
                   ac.addAction(editTime)
                   let editPriorty = UIAlertAction(title: "Edit Priority", style: .default) {
                       [weak self] action in
                       let newAC = UIAlertController(title: "New Priority", message: nil, preferredStyle: .alert)
                       newAC.addTextField(configurationHandler: { textField in
                           textField.keyboardType = .numberPad
                       })
                       let submit = UIAlertAction(title: "Enter", style: .default) {
                           [weak self, weak newAC] action in
                           guard let answer = newAC?.textFields?[0].text else { return }
                         let test = answer.trimmingCharacters(in: .whitespacesAndNewlines)
                                    if test.isEmpty {
                                        self?.classesTable.reloadData()
                                                        return }
                           self?.submitNewPriorty(answer, path: indexPath.row)
                           
                       }
                       newAC.addAction(submit)
                       self?.present(newAC, animated: true)
                       
                   }
                   ac.addAction(editPriorty)
            
            let editNotes = UIAlertAction(title: "Edit Notes", style: .default) {
                [weak self] action in
                let newAC = UIAlertController(title: "New Note", message: nil, preferredStyle: .alert)
               newAC.addTextField()
                let submit = UIAlertAction(title: "Enter", style: .default) {
                    [weak self, weak newAC] action in
                     guard let answer = newAC?.textFields?[0].text else { return }
                    let test = answer.trimmingCharacters(in: .whitespacesAndNewlines)
                        if test.isEmpty {
                            self?.classesTable.reloadData()
                                return }
                     self?.submitNewNotes(answer, path: indexPath.row)
                }
                newAC.addAction(submit)
                self?.present(newAC, animated: true)
            }
            ac.addAction(editNotes)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) {
                [weak self] action in
                self?.classesTable.reloadData()
                
            }
            ac.addAction(cancel)
                
                
                   self.present(ac, animated: true)
                   
               }
        edit.backgroundColor = .blue
           
        let delete = UIContextualAction(style: .normal, title: "Delete") {_,_,_ in
                   
                   classesList.remove(at: indexPath.row)
                   self.save()
                   self.classesTable.reloadData()
               }
               delete.backgroundColor = .red
               
        let viewNotes = UIContextualAction(style: .normal, title: "View Notes") {_,_,_ in
            self.ac(title: "\(classesList[indexPath.row].name!) notes", message: "\(classesList[indexPath.row].notes!)", button: "Done")
                   
               }
              
               
               viewNotes.backgroundColor = UIColor.lightGray
              
        return UISwipeActionsConfiguration(actions: [delete, edit, viewNotes])
    }
    
    func submitNewClass(_ answer: String, path: Int) {
        var oldTime = ""
        var oldPriorty = 0
        oldTime = classesList[path].time!
        oldPriorty = classesList[path].priority!
         let oldNotes = classesList[path].notes!
        classesList.remove(at: path)
        
        classesList.insert(Classes(id: 0, name: answer, time: oldTime, priority: oldPriorty, notes: oldNotes), at: path)
        save()
        classesTable.reloadData()
    }
    func submitNewTime(_ answer: String, path: Int) {
        
        var oldName = ""
        var oldPriorty = 0
        oldName = classesList[path].name!
        oldPriorty = classesList[path].priority!
         let oldNotes = classesList[path].notes!
        classesList.remove(at: path)
        
        classesList.insert(Classes(id: 0, name: oldName, time: answer, priority: oldPriorty, notes: oldNotes), at: path)
        save()
        classesTable.reloadData()
    }
    
    func submitNewPriorty(_ answer: String, path: Int) {
        if Int(answer)! < 1 || Int(answer)! > 10 {
            ac(title: "Invalid", message: "This priority is invalid", button: "OK")
            return
            
        }
        var oldName = ""
        var oldTime = ""
        oldName = classesList[path].name!
        oldTime = classesList[path].time!
        let oldNotes = classesList[path].notes!
        classesList.remove(at: path)
        
        classesList.insert(Classes(id: 0, name: oldName, time: oldTime, priority: Int(answer), notes: oldNotes), at: path)
        save()
        classesTable.reloadData()
    }
    
    func submitNewNotes(_ answer: String, path: Int) {
         let oldName = classesList[path].name!
        let oldTime = classesList[path].time!
        let oldPriority = classesList[path].priority!
        classesList.remove(at: path)
        
        classesList.insert(Classes(id: 0, name: oldName, time: oldTime, priority: Int(oldPriority), notes: answer), at: path)
        save()
        classesTable.reloadData()
    }
    
    
    @IBAction func tap(_ sender: Any) {
        classTimeTextField.resignFirstResponder()
        classNameTextField.resignFirstResponder()
        priorityTextField.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    @IBAction func tfAction(_ sender: UITextField) {
        doneAction()
    }
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
//        popoverPresentationController.sourceView = classesTable.backgroundView
//        popoverPresentationController.sourceRect = CGRect(x: 0, y: 0, width: 1, height: 1)
     
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          let currentText = classNameTextField.text ?? ""
          guard let stringRange = Range(range, in: currentText) else { return false }
          
          let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
          
          return updatedText.count <= 12
      }
    
    
    

    @IBAction func timeEditingChanged(_ sender: UITextField) {
        let tText = classTimeTextField.text
               let timeLastChar = tText?.last
        if timeLastChar != "1" && timeLastChar != "2" && timeLastChar != "3" && timeLastChar != "4" && timeLastChar != "5" && timeLastChar != "6" && timeLastChar != "7" && timeLastChar != "8" && timeLastChar != "9" && timeLastChar != "0"  {
                   var text = tText
            if tText != "" {
                text?.removeLast()
                   classTimeTextField.text = text
            }
               }
    }
    
    @IBAction func pEditingChanged(_ sender: UITextField) {
        let tText = priorityTextField.text
                      let timeLastChar = tText?.last
                      if timeLastChar != "1" && timeLastChar != "2" && timeLastChar != "3" && timeLastChar != "4" && timeLastChar != "5" && timeLastChar != "6" && timeLastChar != "7" && timeLastChar != "8" && timeLastChar != "9" && timeLastChar != "0" {
                          var text = tText
                        if tText != "" {
                       text?.removeLast()
                          priorityTextField.text = text
                        }
                      }
    }
    
    
    
}
