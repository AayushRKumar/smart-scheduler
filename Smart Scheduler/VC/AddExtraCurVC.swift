//
//  AddExtraCurVC.swift
//  Smart Scheduler
//
//  Created by Ankit R Kumar on 7/9/18.
//  Copyright © 2018 CompuBaba. All rights reserved.
//

import UIKit
var extraCurList = [ExtraCur]()

class AddExtraCurVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
   var invalid = false

    @IBOutlet var enterWeak: UIButton!
    @IBOutlet var startPicker: UIDatePicker!
    
    @IBOutlet var endPicker: UIDatePicker!
    override func viewWillAppear(_ animated: Bool)
       
   {
       NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTotalView"), object: nil)

       if #available(iOS 13.0, *) {
       
           if traitCollection.userInterfaceStyle == .dark {
            
               view.backgroundColor = UIColor.black
               
          
           } else {
               view.backgroundColor = UIColor.white
           
           }
       } else {
           view.backgroundColor = UIColor.white
       }
   }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.title = "Extracurriculars"
        
    }
    @IBOutlet weak var activityTable: UITableView!
    @IBOutlet weak var nameOfActivty: UITextField!
   
    
    @IBAction func saveItems(_ sender: Any) {
        doingAction()
        
    }
    func doingAction() {
        
        let nameStorage = nameOfActivty.text?.trimmingCharacters(in: .whitespacesAndNewlines)
      
        if(nameStorage?.isEmpty)!{
            nameOfActivty.layer.borderColor = UIColor.red.cgColor
            ac(title: "Invalid", message: "Please give the extra-curricular a name", button: "OK")
          return
        }
    
        
        if inDarkMode {
            nameOfActivty.layer.borderColor = UIColor.white.cgColor
        } else {
        nameOfActivty.layer.borderColor = UIColor.black.cgColor
        }
        nameOfActivty.text=""
      
        extraCurList.append(ExtraCur(id: 0, name: nameStorage, startTime: startPicker.date, endTime: endPicker.date))
        save()
        
        print("Data saved successfully")
    }
    func ac(title: String, message: String, button: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
        present(alert, animated: true)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return extraCurList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ExtraCurTableViewCell
        let extraCur: ExtraCur
        extraCur = extraCurList[indexPath.row]
        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
               
        let dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: extraCur.startTime)
                 
        var Shour = dateComponents.hour!
         var Sminute = String(dateComponents.minute!)
         
        let dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: extraCur.endTime)
         var EHour = dateComponents2.hour!
         var EMinute = String(dateComponents2.minute!)
          if Int(EMinute)! < 10 {
                                   EMinute = "0\(EMinute)"
                               }
                               if Int(Sminute)! < 10 {
                                   Sminute = "0\(Sminute)"
                               }
        let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
         if dateFormat.firstIndex(of: "a") == nil {
            cell.startLabel.text = "\(Shour):\(Sminute)"
                      cell.endLabel.text = "\(EHour):\(EMinute)"
         } else {
             
                  
            var startAMPM = ""
             if Shour <= 12 {
                 startAMPM = "am"
             } else {
                 Shour -= 12
                 startAMPM = "pm"
             }
             var endAMPM = ""
             if EHour <= 12 {
                 endAMPM = "am"
             } else {
                 EHour -= 12
                 endAMPM = "pm"
             }
             
           
            cell.startLabel.text = "\(Shour):\(Sminute) \(startAMPM)"
           cell.endLabel.text = "\(EHour):\(EMinute) \(endAMPM)"
         }
      
        cell.nameLabel.text = extraCur.name
        return cell
    }
    
    func save() {
        
        let jsonEncoder = JSONEncoder()
        if let saveData = try? jsonEncoder.encode(extraCurList) {
            let defaults = UserDefaults.standard
            print(saveData)
            defaults.set(saveData, forKey: "SYNCextraCur")
        } else {
            print("Failed To save")
        }
        
        
        
  
        self.activityTable.reloadData()
    }
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let edit = UIContextualAction(style: .normal, title: "Edit") { _,_,_ in
                   let ac = UIAlertController(title: "Edit", message: "Choose what you would like to edit", preferredStyle: .actionSheet)
           
            ac.popoverPresentationController?.sourceView = self.activityTable as UIView
            ac.popoverPresentationController?.sourceRect = self.activityTable.rectForRow(at: indexPath)
            
                   let editName = UIAlertAction(title: "Edit Activity Name", style: .default) {
                       [weak self] action in
                       let newAC = UIAlertController(title: "New Name", message: nil, preferredStyle: .alert)
                       newAC.addTextField()
                       let submit = UIAlertAction(title: "Enter", style: .default) {
                           [weak self, weak newAC] action in
                           
                           guard let answer = newAC?.textFields?[0].text else { return }
                           let test = answer.trimmingCharacters(in: .whitespacesAndNewlines)
                           if test.isEmpty {
                               self?.activityTable.reloadData()
                                               return }
                           self?.submitNewName(answer, path: indexPath.row)
                           
                       }
                       newAC.addAction(submit)
                       self?.present(newAC, animated: true)
                       
                   }
                   ac.addAction(editName)
                   let editStart = UIAlertAction(title: "Edit Start Time", style: .default) {
                       [weak self] action in
                       let newAC = UIAlertController(title: "New Start Time", message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
                       
                             
                             //  Create a frame (placeholder/wrapper) for the picker and then create the picker
                             let pickerFrame = CGRect(x: 0, y: 30, width: 270, height: 200) // CGRectMake(left), top, width, height) - left and top are like margins
                             let picker = UIDatePicker(frame: pickerFrame)
                    picker.minimumDate = self?.startPicker.minimumDate
                                       picker.maximumDate = extraCurList[indexPath.row].endTime
                                       picker.date = extraCurList[indexPath.row].startTime
                    
                    picker.datePickerMode = .time
                             newAC.view.addSubview(picker)
                       let submit = UIAlertAction(title: "Enter", style: .default) {
                           [weak self] action in
                           
                        self?.submitNewStart(picker.date, path: indexPath.row)
                           
                       }
                       newAC.addAction(submit)
                       self?.present(newAC, animated: true)
                       
                   }
                   ac.addAction(editStart)
                   let editTime = UIAlertAction(title: "Edit End Time", style: .default) {
                       [weak self] action in
                    let newAC = UIAlertController(title: "New End Time", message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
                       //  Create a frame (placeholder/wrapper) for the picker and then create the picker
                                    let pickerFrame = CGRect(x: 0, y: 30, width: 270, height: 200) // CGRectMake(left), top, width, height) - left and top are like margins
                                let picker = UIDatePicker(frame: pickerFrame)
                                picker.datePickerMode = .time
                    picker.minimumDate = extraCurList[indexPath.row].startTime
                                    picker.maximumDate = self?.endPicker.maximumDate
                                        picker.date = extraCurList[indexPath.row].endTime
                                    newAC.view.addSubview(picker)
                       let submit = UIAlertAction(title: "Enter", style: .default) {
                           [weak self] action in
                          
                        self?.submitNewEnd(picker.date, path: indexPath.row)
                           
                       }
                       newAC.addAction(submit)
                       self?.present(newAC, animated: true)
                       
                   }
                   ac.addAction(editTime)
                   let cancel = UIAlertAction(title: "Cancel", style: .cancel) {
                                  [weak self] action in
                                    self?.activityTable.reloadData()
                              }
                              ac.addAction(cancel)
                   self.present(ac, animated: true)
                   
               }
               edit.backgroundColor = .blue
               
        let delete = UIContextualAction(style: .normal, title: "Delete") {_,_,_ in
              
                   extraCurList.remove(at: indexPath.row)
                   self.save()
                   self.activityTable.reloadData()
               }
               delete.backgroundColor = .red
        return UISwipeActionsConfiguration(actions: [delete, edit])
    }
    
    func submitNewName(_ answer: String, path: Int) {
        
        let oldStart = extraCurList[path].startTime
        let oldEnd = extraCurList[path].endTime
        extraCurList.remove(at: path)
        extraCurList.insert(ExtraCur(id: 0, name: answer, startTime: oldStart, endTime: oldEnd), at: path)
        save()
      activityTable.reloadData()
    }
    
    func submitNewStart(_ newDate: Date, path: Int) {
        
       
        let oldName = extraCurList[path].name
        let oldEnd = extraCurList[path].endTime
        extraCurList.remove(at: path)
        extraCurList.insert(ExtraCur(id: 0, name: oldName, startTime: newDate, endTime: oldEnd), at: path)
        save()
        activityTable.reloadData()
    }
    
    
    func submitNewEnd(_ newDate: Date, path: Int) {
        
       
        let oldName = extraCurList[path].name
        let oldStart = extraCurList[path].startTime
        extraCurList.remove(at: path)
        extraCurList.insert(ExtraCur(id: 0, name: oldName, startTime: oldStart, endTime: newDate), at: path)
        save()
        activityTable.reloadData()
    }
    
   
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        nameOfActivty.delegate = self
        nameOfActivty.layer.borderWidth = 1
        if inDarkMode {
           nameOfActivty.layer.borderColor = UIColor.white.cgColor
        } else {
        nameOfActivty.layer.borderColor = UIColor.black.cgColor
        }
        nameOfActivty.layer.cornerRadius = 10
        nameOfActivty.clipsToBounds = true
        let defaults = UserDefaults.standard
        if let savedPeople = defaults.object(forKey: "SYNCextraCur") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                extraCurList = try jsonDecoder.decode([ExtraCur].self, from: savedPeople)
                if !extraCurList.isEmpty {
                    startPicker.date = extraCurList[0].startTime
                    endPicker.date = extraCurList[0].endTime
                }
              
            } catch {
                print("Failed TO load")
            }
        }
        if let savedClass = defaults.object(forKey: "SYNCschoolHours") as? Data {
                  let jsonDecoder = JSONDecoder()
                  do {
                      schoolHours = try jsonDecoder.decode([SchoolHour].self, from: savedClass)
                    if !schoolHours.isEmpty {
                       
                        startPicker.minimumDate = schoolHours[0].endTime.changeDateToDateToday()
                        
                    }

                  } catch {
                      print("Failed TO load")
                  }
              }
        if let savedBed = defaults.object(forKey: "SYNCbedTime") as? Data {
                       let jsonDecoder = JSONDecoder()
                       do {
                           bedTime = try jsonDecoder.decode([BedTime].self, from: savedBed)
                        if !bedTime.isEmpty {
                            startPicker.maximumDate = bedTime[0].startTime.changeDateToDateToday()
                           
                            endPicker.maximumDate = startPicker.maximumDate
                        }
                       } catch {
                           print("Failed TO load")
                       }
                   }
                  endPicker.minimumDate = startPicker.date
//
//        }
      
        
    }
    
    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        nameOfActivty.resignFirstResponder()
     
    }
    @IBAction func nameTFAction(_ sender: UITextField) {
        doingAction()
    }
    
    @IBAction func startTFAction(_ sender: UITextField) {
        doingAction()
    }
    

    @IBAction func endTFAction(_ sender: UITextField) {
        doingAction()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @IBAction func startPickerAction(_ sender: UIDatePicker) {
        endPicker.minimumDate = startPicker.date
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
             let currentText = nameOfActivty.text ?? ""
             guard let stringRange = Range(range, in: currentText) else { return false }
             
             let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
             
             return updatedText.count <= 12
         }
}

