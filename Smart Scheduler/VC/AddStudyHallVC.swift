//
//  AddStudyHallVC.swift
//  Smart Scheduler
//
//  Created by Ankit R Kumar on 8/5/18.
//  Copyright © 2018 CompuBaba. All rights reserved.
//

import UIKit
 var studyHallList = [StudyHall]()

class AddStudyHallVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var invalid = false
    
  
    @IBOutlet var endPicker: UIDatePicker!
    @IBOutlet var startPicker: UIDatePicker!
    override func viewWillAppear(_ animated: Bool)
      
  {
      NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTotalView"), object: nil)

      if #available(iOS 13.0, *) {
      
          if traitCollection.userInterfaceStyle == .dark {
           
              view.backgroundColor = UIColor.black
         
         
          } else {
              view.backgroundColor = UIColor.white
          
          }
      } else {
          view.backgroundColor = UIColor.white
      }
  }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.title = "Study Halls"
    }
    
    @IBOutlet weak var studyTable: UITableView!
 
    
  
    @IBAction func done(_ sender: Any) {
        doneAction()
    
    }
    func doneAction() {
        
        
        studyHallList.append(StudyHall(id: 0, startTime: startPicker.date, endTime: endPicker.date))
       
        save()
        
        print("Data saved successfully")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studyHallList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StudyHallTableViewCell
           let hall: StudyHall
        hall = studyHallList[indexPath.row]
           let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                  
        let dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: hall.startTime)
                    
           var Shour = dateComponents.hour!
            var Sminute = String(dateComponents.minute!)
            
        let dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: hall.endTime)
            var EHour = dateComponents2.hour!
            var EMinute = String(dateComponents2.minute!)
             if Int(EMinute)! < 10 {
                                      EMinute = "0\(EMinute)"
                                  }
                                  if Int(Sminute)! < 10 {
                                      Sminute = "0\(Sminute)"
                                  }
           let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
            if dateFormat.firstIndex(of: "a") == nil {
               cell.startLabel.text = "\(Shour):\(Sminute)"
                         cell.endLabel.text = "\(EHour):\(EMinute)"
            } else {
                
                     
               var startAMPM = ""
                var endAMPM = ""
                if Shour == 12 {
                    startAMPM = "pm"
                } else {
                    if Shour <= 12 {
                                       startAMPM = "am"
                                   } else {
                                       Shour -= 12
                                       startAMPM = "pm"
                                   }
                }
                if EHour == 12 {
                    endAMPM = "pm"
                } else {
                    if EHour <= 12 {
                                endAMPM = "am"
                                  } else {
                                      EHour -= 12
                                      endAMPM = "pm"
                                  }
                                  
                }
               
                
              
              
               cell.startLabel.text = "\(Shour):\(Sminute) \(startAMPM)"
              cell.endLabel.text = "\(EHour):\(EMinute) \(endAMPM)"
            }
        return cell
    }
    
    func ac(title: String, message: String, button: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
        present(alert, animated: true)
        
    }
    
    func save(){
        
        let jsonEncoder = JSONEncoder()
        if let saveData = try? jsonEncoder.encode(studyHallList) {
            let defaults = UserDefaults.standard
            defaults.set(saveData, forKey: "SYNCstudyHalls")
        } else {
            print("Failed TO load")
        }
        
      
        
        
        
       
        
        self.studyTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        let defaults = UserDefaults.standard
        if let savedHall = defaults.object(forKey: "SYNCstudyHalls") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                studyHallList = try jsonDecoder.decode([StudyHall].self, from: savedHall)
                
                
            } catch {
                print("Failed TO load")
            }
        }
      if let savedClass = defaults.object(forKey: "SYNCschoolHours") as? Data {
                       let jsonDecoder = JSONDecoder()
                       do {
                           schoolHours = try jsonDecoder.decode([SchoolHour].self, from: savedClass)
                         if !schoolHours.isEmpty {
                            
                         startPicker.minimumDate = schoolHours[0].startTime
                             startPicker.maximumDate = schoolHours[0].endTime
                            endPicker.minimumDate = schoolHours[0].startTime
                                endPicker.maximumDate = schoolHours[0].endTime
                         }

                       } catch {
                           print("Failed TO load")
                       }
                   }
        endPicker.minimumDate = startPicker.date
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last! as String
        print(documentPath)

        
    }
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
   
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = UIContextualAction(style: .normal, title: "Edit") {_,_,_ in
                   let ac = UIAlertController(title: "Edit", message: "Choose what you would like to edit", preferredStyle: .actionSheet)
                   let editName = UIAlertAction(title: "Edit Start Time", style: .default) {
                       [weak self] action in
                       let newAC = UIAlertController(title: "New Start Time", message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
                        let pickerFrame = CGRect(x: 0, y: 30, width: 270, height: 200) // CGRectMake(left), top, width, height) - left and top are like margins
                          
                                    let picker = UIDatePicker(frame: pickerFrame)
                            picker.datePickerMode = .time
                    picker.minimumDate = self?.startPicker.minimumDate
                    picker.maximumDate = studyHallList[indexPath.row].endTime
                    picker.date = studyHallList[indexPath.row].startTime
                                                  newAC.view.addSubview(picker)
                       let submit = UIAlertAction(title: "Enter", style: .default) {
                           [weak self] action in
                           
                        
                           
                        self?.submitNewStart(picker.date, path: indexPath.row)
                           
                       }
                       newAC.addAction(submit)
                       self?.present(newAC, animated: true)
                       
                   }
                   ac.addAction(editName)
                   let editTime = UIAlertAction(title: "Edit End Time", style: .default) {
                       [weak self] action in
                       
                        let newAC = UIAlertController(title: "New End Time", message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
                                    let pickerFrame = CGRect(x: 0, y: 30, width: 270, height: 200) // CGRectMake(left), top, width, height) - left and top are like margins
                                    let picker = UIDatePicker(frame: pickerFrame)
                                picker.datePickerMode = .time
                    picker.minimumDate = studyHallList[indexPath.row].startTime
                                      picker.maximumDate = self?.endPicker.maximumDate
                                      picker.date = studyHallList[indexPath.row].endTime
                                    newAC.view.addSubview(picker)
                       let submit = UIAlertAction(title: "Enter", style: .default) {
                           [weak self] action in
                          
                        self?.submitNewEnd(picker.date, path: indexPath.row)
                           
                       }
                       newAC.addAction(submit)
                       self?.present(newAC, animated: true)
                       
                   }
                   ac.addAction(editTime)
                   
                  let cancel = UIAlertAction(title: "Cancel", style: .cancel) {
                                  [weak self] action in
                                  self?.studyTable.reloadData()
                                  
                              }
                              ac.addAction(cancel)
                                  
                   self.present(ac, animated: true)
                   
               }
               edit.backgroundColor = .blue
               
        let delete = UIContextualAction(style: .normal, title: "Delete") {_,_,_ in 
                   
                   studyHallList.remove(at: indexPath.row)
                   self.save()
                   self.studyTable.reloadData()
               }
               delete.backgroundColor = .red
               
        return UISwipeActionsConfiguration(actions: [delete, edit])
              
    }
    
    
    
  
    func submitNewStart(_ answer: Date, path: Int) {
       
        var oldEndTime = Date()
      
        oldEndTime = studyHallList[path].endTime
        studyHallList.remove(at: path)
        
        studyHallList.insert(StudyHall(id: 0, startTime: answer, endTime: oldEndTime), at: path)
        save()
        studyTable.reloadData()
    }
    
    func submitNewEnd(_ answer: Date, path: Int) {
       
      
        var oldStartTime = Date()
       
        oldStartTime = studyHallList[path].startTime
        studyHallList.remove(at: path)
        
        studyHallList.insert(StudyHall(id: 0, startTime: oldStartTime, endTime: answer), at: path)
        save()
        studyTable.reloadData()
    }
    
    
    
    
  
    
    
    
    
    
    
    

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    
    @IBAction func startPickerAction(_ sender: UIDatePicker) {
        endPicker.minimumDate = startPicker.date
    }
    
    
    
    
    
    
}
