//
//  BedTimeVC.swift
//  Smart Scheduler
//
//  Created by Aayush R Kumar on 7/29/19.
//  Copyright © 2019 CompuBaba. All rights reserved.
//

import UIKit
var bedTime = [BedTime]()
class BedTimeVC: UIViewController {
  
    
 
    @IBOutlet var startPicker: UIDatePicker!
    
    @IBOutlet var endPicker: UIDatePicker!
    
    @IBOutlet var hourLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.title = "Bed Time"
         
    }
    
    override func viewWillAppear(_ animated: Bool)
        
    {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTotalView"), object: nil)

        if #available(iOS 13.0, *) {
            if traitCollection.userInterfaceStyle == .dark {
             
                view.backgroundColor = UIColor.black
           
           
            } else {
                view.backgroundColor = UIColor.white
            
            }
        } else {
            view.backgroundColor = UIColor.white
        }
       
    }
   
    
    
  
    
  
    
    func ac(title: String, message: String, button: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
        present(alert, animated: true)
        
    }
    
    @IBAction func submit(_ sender: UIButton) {
        doneAction()
    }
    
    func doneAction() {
      
        bedTime.removeAll()
           
           
           bedTime.append(BedTime(id: 0, startTime: startPicker.date, endTime: endPicker.date))
        
           

           reloadLabel()
           save()
        
      //  bedTime.append(BedTime(id: 0, startTime: startTime!, endTime: endTime!, startAMPM: startAMPN, endAMPM: endAMPN))
      
        
 
    }
  
    
    func save() {
        
        
        let jsonEncoder = JSONEncoder()
        if let saveData = try? jsonEncoder.encode(bedTime) {
            let defaults = UserDefaults.standard
            defaults.set(saveData, forKey: "SYNCbedTime")
        } else {
            print("Failed TO load")
        }
        
        
   
        
        
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      hourLabel.adjustsFontSizeToFitWidth = true
        let defaults = UserDefaults.standard
        if let savedBed = defaults.object(forKey: "SYNCbedTime") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                bedTime = try jsonDecoder.decode([BedTime].self, from: savedBed)
                if !bedTime.isEmpty {
                startPicker.date = bedTime[0].startTime
                endPicker.date = bedTime[0].endTime
                }
               
                      reloadLabel()
                
            } catch {
                print("Failed TO load")
            }
        } else {
              let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
            var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
                   dateComponents.hour = 21
                   dateComponents.minute = 0
                    var dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
                   dateComponents2.hour = 6
                   dateComponents2.minute = 0
            
            startPicker.date = Calendar.current.date(from: dateComponents)!
            endPicker.date = Calendar.current.date(from: dateComponents2)!
        }
      
                            
                     
     
        
    }
    
    func reloadLabel() {
           if !bedTime.isEmpty {
                 let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                     
               let dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: bedTime[0].startTime)
                       
              var Shour = dateComponents.hour!
               var Sminute = String(dateComponents.minute!)
               
               let dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: bedTime[0].endTime)
               var EHour = dateComponents2.hour!
               var EMinute = String(dateComponents2.minute!)
                if Int(EMinute)! < 10 {
                                         EMinute = "0\(EMinute)"
                                     }
                                     if Int(Sminute)! < 10 {
                                         Sminute = "0\(Sminute)"
                                     }
              let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
               if dateFormat.firstIndex(of: "a") == nil {
                  hourLabel.text = "\(Shour):\(Sminute) - \(EHour):\(EMinute)"
               } else {
                   
                        
                  var startAMPM = ""
                   if Shour <= 12 {
                       startAMPM = "am"
                   } else {
                       Shour -= 12
                       startAMPM = "pm"
                   }
                   var endAMPM = ""
                   if EHour <= 12 {
                       endAMPM = "am"
                   } else {
                       EHour -= 12
                       endAMPM = "pm"
                   }
                   
                   hourLabel.text = "\(Shour):\(Sminute) \(startAMPM) - \(EHour):\(EMinute) \(endAMPM)"
               }
             
             
           }
       }
    
    
  
    
    @IBAction func startAction(_ sender: UIDatePicker) {
        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
               
               
               
        var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: startPicker.date)
        if dateComponents.hour! >= 0 && dateComponents.hour! < 12 {
       
            var newDateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
            newDateComponents.hour = 23
            newDateComponents.minute = 0
            startPicker.date = (Calendar.current.date(from: newDateComponents)?.changeDateToDateToday())!
        }
        
    }
    
    
    @IBAction func endAction(_ sender: UIDatePicker) {
        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                      
                      
                      
               var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: endPicker.date)
               if dateComponents.hour! > 11   {
                 
                   var newDateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
                   newDateComponents.hour = 6
                   newDateComponents.minute = 0
                   endPicker.date = (Calendar.current.date(from: newDateComponents)?.changeDateToDateToday())!
               }
    }
    
}
