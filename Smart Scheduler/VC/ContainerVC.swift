//
//  ViewController.swift
//  Smart Scheduler
//
//  Created by Ankit R Kumar on 7/1/18.
//  Copyright © 2018 CompuBaba. All rights reserved.
//

import UIKit

class ContainerVC: UIViewController {
    
    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    
    var sideMenuOpen = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self , selector: #selector(toggleSideMenu ), name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool)
             
         {
          NotificationCenter.default.addObserver(self, selector: #selector(disconnectPaxiSocket(_:)), name: Notification.Name(rawValue: "updateTotalView"), object: nil)
           
             if #available(iOS 13.0, *) {
                 if traitCollection.userInterfaceStyle == .dark {
                  
                     view.backgroundColor = UIColor.black
                
                     
                 } else {
                     view.backgroundColor = UIColor.white
                 
                 }
             } else {
                 view.backgroundColor = UIColor.white
             }
         }
    func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
       print("VIEW WILL DISSAPPERAR")
    }
    @objc func disconnectPaxiSocket(_ notification: Notification) {
     if #available(iOS 13.0, *) {
                    if traitCollection.userInterfaceStyle == .dark {
                     
                        view.backgroundColor = UIColor.black
                   
                        
                    } else {
                        view.backgroundColor = UIColor.white
                    
                    }
                } else {
                    view.backgroundColor = UIColor.white
                }
    }
    @objc func toggleSideMenu()
    {
        if sideMenuOpen
        {
            sideMenuOpen = false
            sideMenuConstraint.constant = -240
        }
        else
        {
            sideMenuOpen = true
            sideMenuConstraint.constant = 0
        }
        UIView.animate(withDuration: 0.3)
        {
            self.view.layoutIfNeeded()
        }
        
    }

}

