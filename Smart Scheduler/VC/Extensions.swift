import Foundation
import UIKit
extension UIViewController {
    func setStatusBarStyle(_ style: UIStatusBarStyle) {
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor = style == .lightContent ? UIColor.black : .white
            statusBar.setValue(style == .lightContent ? UIColor.white : .black, forKey: "foregroundColor")
        }
    }
}
var dayTomorrow = 0
var monthTommorrow = 0
var yearTommorrow = 0
extension Date {
    func changeDateToDateToday() -> Date {
        let calendar = Calendar.current
               
               let date = Date()
               let day = calendar.component(.day, from: date)
               let month = calendar.component(.month, from: date)
               let year = calendar.component(.year, from: date)
            let dayToday = day
               let monthToday = month
              let yearToday = year
        
        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                             
            var startDateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: self)
                startDateComponents.year = yearToday
                startDateComponents.month = monthToday
              startDateComponents.day = dayToday
        startDateComponents.second = 0
        return Calendar.current.date(from: startDateComponents)!
    }
    func changeDateToDateTommorow() -> Date {
        generateDateTomorrow()
        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                                   
        var startDateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: self)
        startDateComponents.year = yearTommorrow
        startDateComponents.month = monthTommorrow
        startDateComponents.day = dayTomorrow
        startDateComponents.second = 0
        return Calendar.current.date(from: startDateComponents)!
    }
}
 func generateDateTomorrow() {
    let calendar = Calendar.current
    
    let date = Date()
    var day = calendar.component(.day, from: date)
    var month = calendar.component(.month, from: date)
    var year = calendar.component(.year, from: date)
    switch month {
      case 1:
        if day == 31 {
            month += 1
            day = 1
        } else {
            day += 1
        }
    case 2:
        if day == 28 {
            month += 1
            day = 1
        } else {
            day += 1
        }
    case 3:
        if day == 31 {
            month += 1
            day = 1
        } else {
            day += 1
        }
    case 4:
        if day == 30 {
            month += 1
            day = 1
        } else {
            day += 1
        }
    case 5:
        if day == 31 {
            month += 1
            day = 1
        } else {
            day += 1
        }
    case 6:
        if day == 30 {
            month += 1
            day = 1
        } else {
            day += 1
        }
    case 7:
        if day == 31 {
            month += 1
            day = 1
        } else {
            day += 1
        }
    case 8:
        if day == 31 {
            month += 1
            day = 1
        } else {
            day += 1
        }
    case 9:
    if day == 30 {
        month += 1
        day = 1
    } else {
        day += 1
        }
    
    case 10:
    if day == 31 {
        month += 1
        day = 1
    } else {
        day += 1
    }
    
    case 11:
        if day == 30 {
            month += 1
            day = 1
        } else {
            day += 1
        }
    case 12:
        if day == 31 {
            year += 1
            month = 1
            day = 1
        } else {
            day += 1
        }
    default:
       print("error date tommorow in extensions")
    }
    dayTomorrow = day
    monthTommorrow = month
    yearTommorrow = year
    
}
extension Date {
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
}
import SystemConfiguration

public class Reachability {

    class func isConnectedToNetwork() -> Bool {

        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }

        /* Only Working for WIFI
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired

        return isReachable && !needsConnection
        */

        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)

        return ret

    }
}
