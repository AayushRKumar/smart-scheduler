//
//  FreeTimeVC.swift
//  Smart Scheduler
//
//  Created by Aayush R Kumar on 7/27/19.
//  Copyright © 2019 CompuBaba. All rights reserved.
//

import UIKit
var setRelaxTimes = [RelaxSetTime]()
var relaxTime = [RelaxTime]()
class FreeTimeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    var pickerComponents = ["Any", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerComponents.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerComponents[row]
    }
   var invalid = false
    
    @IBOutlet var enterWeak: UIButton!
    
    @IBOutlet var startPicker: UIDatePicker!
    @IBOutlet var endPicker: UIDatePicker!
    
    @IBOutlet var scrollView: UIView!
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var pickerView: UIPickerView!
    @IBOutlet var minutesField: UITextField!
    @IBOutlet var freeTable: UITableView!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.title = "Free Time"
    }
  override func viewWillAppear(_ animated: Bool)
           
       {
           NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTotalView"), object: nil)

           if #available(iOS 13.0, *) {
            
               if traitCollection.userInterfaceStyle == .dark {
                
                   view.backgroundColor = UIColor.black
                scrollView.backgroundColor = UIColor.black
              
               } else {
                   view.backgroundColor = UIColor.white
                scrollView.backgroundColor = UIColor.white
               }
           } else {
               view.backgroundColor = UIColor.white
             scrollView.backgroundColor = UIColor.white
           }
       }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        pickerView.delegate = self
        pickerView.dataSource = self
        minutesField.layer.cornerRadius = 10
        minutesField.clipsToBounds = true
        let defaults = UserDefaults.standard
        if let savedSet = defaults.object(forKey: "SYNCsetRelaxTimes") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                setRelaxTimes = try jsonDecoder.decode([RelaxSetTime].self, from: savedSet)
                if !setRelaxTimes.isEmpty {
                startPicker.date = setRelaxTimes[0].startTime
                endPicker.date = setRelaxTimes[0].endTime
                }
            } catch {
                print("Failed TO load")
            }
        }
            
            if let savedSet = defaults.object(forKey: "SYNCchunkedRelaxTime") as? Data {
                let jsonDecoder = JSONDecoder()
                do {
                    relaxTime = try jsonDecoder.decode([RelaxTime].self, from: savedSet)
                    
                } catch {
                    print("Failed TO load")
                }
            }
          if let savedClass = defaults.object(forKey: "SYNCschoolHours") as? Data {
                          let jsonDecoder = JSONDecoder()
                          do {
                              schoolHours = try jsonDecoder.decode([SchoolHour].self, from: savedClass)
                            if !schoolHours.isEmpty {
                               
                                startPicker.minimumDate = schoolHours[0].endTime.changeDateToDateToday()
                                
                            }

                          } catch {
                              print("Failed TO load")
                          }
                      }
                if let savedBed = defaults.object(forKey: "SYNCbedTime") as? Data {
                               let jsonDecoder = JSONDecoder()
                               do {
                                   bedTime = try jsonDecoder.decode([BedTime].self, from: savedBed)
                                if !bedTime.isEmpty {
                                    startPicker.maximumDate = bedTime[0].startTime.changeDateToDateToday()
                                    
                                    endPicker.maximumDate = startPicker.maximumDate
                                }
                               } catch {
                                   print("Failed TO load")
                               }
                           }
          endPicker.minimumDate = startPicker.date
        //
            reloadDetail()
       if inDarkMode {
                 minutesField.layer.borderColor = UIColor.white.cgColor
              } else {
             minutesField.layer.borderColor = UIColor.black.cgColor
              }
       
        minutesField.layer.borderWidth = 1
    
    }
    func reloadDetail() {
        if relaxTime.count > 0 {
        detailLabel.text = "\(relaxTime[0].time) minutes broken up into \(relaxTime[0].sessions) chunks"
        }
    }
    
   
    @IBAction func submit(_ sender: UIButton) {
        doneSetAction()
    }
    func doneSetAction() {
       
        setRelaxTimes.append(RelaxSetTime(startTime: startPicker.date, endTime: endPicker.date, name: "Free Time"))
      
        saveSet()
    }
    func saveSet() {
        let jsonEncoder = JSONEncoder()
        if let saveData = try? jsonEncoder.encode(setRelaxTimes) {
            let defaults = UserDefaults.standard
            defaults.set(saveData, forKey: "SYNCsetRelaxTimes")
        } else {
            print("Failed TO load")
        }
        
        
        self.freeTable.reloadData()
    }
    
    func ac(title: String, message: String, button: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
        present(alert, animated: true)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setRelaxTimes.count
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RelaxTableViewCell
           let time: RelaxSetTime
        time = setRelaxTimes[indexPath.row]
           let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                  
        let dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: time.startTime)
                    
           var Shour = dateComponents.hour!
            var Sminute = String(dateComponents.minute!)
            
           let dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: time.endTime)
            var EHour = dateComponents2.hour!
            var EMinute = String(dateComponents2.minute!)
             if Int(EMinute)! < 10 {
                                      EMinute = "0\(EMinute)"
                                  }
                                  if Int(Sminute)! < 10 {
                                      Sminute = "0\(Sminute)"
                                  }
           let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
            if dateFormat.firstIndex(of: "a") == nil {
               cell.startLabel.text = "\(Shour):\(Sminute)"
                         cell.endLabel.text = "\(EHour):\(EMinute)"
            } else {
                
                     
               var startAMPM = ""
                if Shour <= 12 {
                    startAMPM = "am"
                } else {
                    Shour -= 12
                    startAMPM = "pm"
                }
                var endAMPM = ""
                if EHour <= 12 {
                    endAMPM = "am"
                } else {
                    EHour -= 12
                    endAMPM = "pm"
                }
                
              
               cell.startLabel.text = "\(Shour):\(Sminute) \(startAMPM)"
              cell.endLabel.text = "\(EHour):\(EMinute) \(endAMPM)"
            }
         
          
           return cell
       }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return true
    }
   
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
           
        let edit = UIContextualAction(style: .normal, title: "Edit") {_,_,_ in
                 let ac = UIAlertController(title: "Edit", message: "Choose what you would like to edit", preferredStyle: .actionSheet)
             ac.popoverPresentationController?.sourceView = self.freeTable as UIView
                      ac.popoverPresentationController?.sourceRect = self.freeTable.rectForRow(at: indexPath)
                 let editName = UIAlertAction(title: "Edit Start Time", style: .default) {
                     [weak self] action in
                     let newAC = UIAlertController(title: "New Start Time", message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
                       
                        //  Create a frame (placeholder/wrapper) for the picker and then create the picker
                        let pickerFrame = CGRect(x: 0, y: 30, width: 270, height: 200) // CGRectMake(left), top, width, height) - left and top are like margins
                                let picker = UIDatePicker(frame: pickerFrame)
                                picker.datePickerMode = .time
                    picker.minimumDate = self?.startPicker.minimumDate
                                       picker.maximumDate =  setRelaxTimes[indexPath.row].endTime
                                       picker.date =  setRelaxTimes[indexPath.row].startTime
                                newAC.view.addSubview(picker)
                     let submit = UIAlertAction(title: "Enter", style: .default) {
                                               [weak self] action in
                                               
                                            self?.submitNewStart(picker.date, path: indexPath.row)
                                              
                                           }
                     newAC.addAction(submit)
                     self?.present(newAC, animated: true)
                     
                 }
                 ac.addAction(editName)
                 let editTime = UIAlertAction(title: "Edit End Time", style: .default) {
                     [weak self] action in
                     let newAC = UIAlertController(title: "New End Time", message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
                      //  Create a frame (placeholder/wrapper) for the picker and then create the picker
                                let pickerFrame = CGRect(x: 0, y: 30, width: 270, height: 200) // CGRectMake(left), top, width, height) - left and top are like margins
                                    let picker = UIDatePicker(frame: pickerFrame)
                                        picker.datePickerMode = .time
                                picker.minimumDate = setRelaxTimes[indexPath.row].startTime
                                                        picker.maximumDate = self?.endPicker.maximumDate
                                                        picker.date = setRelaxTimes[indexPath.row].endTime
                                    newAC.view.addSubview(picker)
                                let submit = UIAlertAction(title: "Enter", style: .default) {
                                                [weak self] action in
                                                                
                                        self?.submitNewEnd(picker.date, path: indexPath.row)
                                                                   
                            }
                     newAC.addAction(submit)
                     self?.present(newAC, animated: true)
                     
                 }
                 ac.addAction(editTime)
                 
                let cancel = UIAlertAction(title: "Cancel", style: .cancel) {
                                [weak self] action in
                                  self?.freeTable.reloadData()
                            }
                            ac.addAction(cancel)
                 self.present(ac, animated: true)
                 
             }
             edit.backgroundColor = .blue
             
        let delete = UIContextualAction(style: .normal, title: "Delete") {_,_,_ in 
                 
                 setRelaxTimes.remove(at: indexPath.row)
                 self.saveSet()
                 self.freeTable.reloadData()
             }
             delete.backgroundColor = .red
             
            return UISwipeActionsConfiguration(actions: [delete, edit])
    }
    func submitNewStart(_ answer: Date, path: Int) {
       
        var oldEnd = Date()
        oldEnd = setRelaxTimes[path].endTime
        setRelaxTimes.remove(at: path)
        setRelaxTimes.insert(RelaxSetTime(startTime: answer, endTime: oldEnd, name: "Free Time"), at: path)
        saveSet()
        freeTable.reloadData()
    }
    
    
    func submitNewEnd(_ answer: Date, path: Int) {
        
        var oldStart = Date()
        oldStart = setRelaxTimes[path].startTime
        setRelaxTimes.remove(at: path)
        setRelaxTimes.insert(RelaxSetTime(startTime: oldStart, endTime: answer, name: "Free Time"), at: path)
        saveSet()
        freeTable.reloadData()
    }
    
    @IBAction func startAction(_ sender: UITextField) {
        doneSetAction()
        
    }
    
    @IBAction func endAction(_ sender: UITextField) {
        doneSetAction()
    }
    
    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
       
        minutesField.resignFirstResponder()
    }
    
    
    @IBAction func submitSessions(_ sender: UIButton) {
        
        let minutes = minutesField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if (minutes?.isEmpty)! {
            minutesField.layer.borderColor = UIColor.red.cgColor
            ac(title: "Invalid", message: "Please fill all boxes", button: "OK")
            return
        }
        
        let selectedValue = pickerView.selectedRow(inComponent: 0)
        relaxTime.removeAll()
        
        guard let time = minutes else {
            ac(title: "Error", message: "Could not load Number from Text", button: "OK")
            return }
        if selectedValue != 0 {
           
        
            relaxTime.append(RelaxTime(time: Int(time)!, sessions: pickerComponents[selectedValue]))
        }  else {
            relaxTime.append(RelaxTime(time: Int(time)!, sessions: "Any Number"))
        }
         if inDarkMode {
                       minutesField.layer.borderColor = UIColor.white.cgColor
                    } else {
                   minutesField.layer.borderColor = UIColor.black.cgColor
                    }
        minutesField.text = ""
        saveSessions()
    }
    
    func saveSessions() {
        let jsonEncoder = JSONEncoder()
        if let saveData = try? jsonEncoder.encode(relaxTime) {
            let defaults = UserDefaults.standard
            defaults.set(saveData, forKey: "SYNCchunkedRelaxTime")
        } else {
            print("Failed TO load")
        }
       reloadDetail()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @IBAction func startPickerAction(_ sender: UIDatePicker) {
        endPicker.minimumDate = startPicker.date
    }
    
    
}
