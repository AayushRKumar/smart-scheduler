//
//  MainVC.swift
//  Smart Scheduler
//
//  Created by Ankit R Kumar on 7/1/18.
//  Copyright © 2018 CompuBaba. All rights reserved.
//

import UIKit
import EventKit
import UserNotifications
import GoogleMobileAds
var sideShowing = false
var inDarkMode = false
var stopAccessToEverythingButBedTime = false
var stopAccessToEverythingButSchoolHours = false
class MainVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UNUserNotificationCenterDelegate, GADInterstitialDelegate
{
    var doneWithFunction = false
     var plantedSuccessfully = false
    var algorthimGeneratedEvents = [EKEvent]()
    var eventsToDelete = [EventToDelete]()
    var dayTomorrow = 0
    var monthTommorrow = 0
    var yearTommorrow = 0
    var dayToday = 0
    var monthToday = 0
    var yearToday = 0
    var generateAfteroon = false
    var userCalendar: EKCalendar!
    let eventStore = EKEventStore()
    var calendar: EKCalendar!
    var writeToBasicsDone = false
    var classesListForEvents = classesList
    var minutesOfClassInStudyHall = 0
    var minutesOfChunkedRelaxTime = 0
    var numberOfSessions = 0
    var startDateOfThingsAfterSchool = [Date]()
    var endDateOfThingsAfterSchool = [Date]()
    var endOfSchoolHours = Date()
    var startOfBedTime = Date()
    var classesInStudyHalls = [StudyHallClass]()
    var checkSchoolHours = false
    var checkBedTime = false
    var checkClass = false
    var checkCal = false
    var freeTimeTime = 0
    var IACounter = 0
    var alertLine1 = ""
    var alertLine2 = ""
    var alertLine3 = ""
    var alertLineTest = ""
    var alertLineTestStr = ""
     var oldNumbSessions = ""
    var userCalendarArray = [userCal]()
     var checkMarkList = ["School Hours", "Bed Time", "At Least One Class","Valid Calendar"]
    var optionsList = ["Push Notifications", "Dynamic Scheduling", "iCloud Sync"]
    var listOfUserCalendars = [String]()
    var notiEnabled = userNotifications(enabled: false)
    var icloudEnabled = saveICloudSync(enabled: false)
    var dynEnabled = saveDynScheduling(enabled: false)
    var schoolHourEndDate = Date()
    var eventIsBeforeSchoolHours = false
    var makeViaNoti = false
    var schoolSchedulingDone = false
    var bedTimeNightSchedulingDone = false
    var testBedStart = Date()
    var FTPO = false
    //For while Counters
    var whileCounter229 = 0
    var whileCounter260 = 0
    var whileCounter1016 = 0
    var whileCounter1039 = 0
    var whileCounter1070 = 0
    var doubleActivites = false
    var doubleActivitesRangeTitle = ""
    var doubleActivitesBetweenTitle = ""
    var whileAdShowing = true
    var publicDyn = false
    //International Array Counter
    var arrayOfClassesTooMuch = [String]()
     var interstitial: GADInterstitial!
    var iCloudSync = true
    @IBOutlet var checkTable: UITableView!
    @IBOutlet var generateWeak: UIButton!
    @IBOutlet var calendarButtonText: UIButton!
    
   
    override func viewWillAppear(_ animated: Bool)
       
   {
    
       NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTotalView"), object: nil)

       if #available(iOS 13.0, *) {
   
        
           if traitCollection.userInterfaceStyle == .dark {
            
               view.backgroundColor = UIColor.black
          
          inDarkMode = true
           } else {
               view.backgroundColor = UIColor.white
            inDarkMode = false
           }
       } else {
       
           view.backgroundColor = UIColor.white
         inDarkMode = false
       }
   }
   
    override var preferredStatusBarStyle : UIStatusBarStyle {
           return UIStatusBarStyle.lightContent
           //return UIStatusBarStyle.default   // Make dark again
       }
    override func viewDidLoad() {
        
        super.viewDidLoad()
         interstitial = createAndLoadInterstitial()
          interstitial.delegate = self
        
        
            self.eventStore.requestAccess(to: EKEntityType.event, completion:
            {(granted, error) in
                if !granted {
                    DispatchQueue.main.async {
                        
                    
                    self.ac(title: "Issue", message: "We do not have access to your calendar. Please enable it in settings. More information can be found in our Privacy Policy.", button: "OK")
                    }
                    return
                }
  })
        
        for (key, value) in UserDefaults.standard.dictionaryRepresentation() {
          print("\(key) = \(value) \n")        }
        NotificationCenter.default.addObserver(self , selector: #selector(ShowClasses), name: NSNotification.Name("ShowClasses"), object: nil)
        NotificationCenter.default.addObserver(self , selector: #selector(ShowSchoolHours), name: NSNotification.Name("ShowSchoolHours"), object: nil)
        NotificationCenter.default.addObserver(self , selector: #selector(ShowStudyHalls), name: NSNotification.Name("ShowStudyHalls"), object: nil)
        NotificationCenter.default.addObserver(self , selector: #selector(ShowExtraActv), name: NSNotification.Name("ShowExtraActv"), object: nil)
        NotificationCenter.default.addObserver(self , selector: #selector(ShowRelaxTime), name: NSNotification.Name("ShowRelaxTime"), object: nil)
        NotificationCenter.default.addObserver(self , selector: #selector(ShowBedTime), name: NSNotification.Name("ShowBedTime"), object: nil)
          NotificationCenter.default.addObserver(self , selector: #selector(ShowHelp), name: NSNotification.Name("ShowHelp"), object: nil)
          NotificationCenter.default.addObserver(self , selector: #selector(ShowContact), name: NSNotification.Name("ShowContact"), object: nil)
         NotificationCenter.default.addObserver(self , selector: #selector(ShowInfo), name: NSNotification.Name("ShowInfo"), object: nil)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainTableViewCell
        loadJSON()
        cell.label.text = optionsList[indexPath.row]
        cell.buttonAction = { sender in
            switch indexPath.row {
            case 0:
            
                self.ac(title: "Push Notifications Info", message: "Push Notifications will send you notifications on when you should start working on homework. They can be disabled at any time.", button: "OK")
            case 1:
                self.ac(title: "Dynamic Scheduling Info", message: "Push Notifications are required for Dynamic Scheduling. In Dynamic Scheduling, if you press a push notification and launch the app, it will prompt you if you finished the PREVIOUS class. If you have not, you can enter how much more time you need for the previous class and continue working on it.", button: "OK")
                       case 2:
                        self.ac(title: "iCloud Sync Info", message: "Turning this on will link the same data (School Hours, Homework, etc) to other devices with the same iCloud Account.", button: "OK")
            default:
                self.ac(title: "Error", message: "Error loading info button.", button: "OK")
            }
        }
        
        cell.switchAction = { sender in
            self.loadJSON()
            switch indexPath.row {
            case 0:
                if (sender as AnyObject).isOn {
                    self.notiEnabled = userNotifications(enabled: true)
                           let center = UNUserNotificationCenter.current()

                           center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                               if granted {
                                    let jsonEncoder = JSONEncoder()
                                self.notiEnabled = userNotifications(enabled: true)
                                   if let saveData = try? jsonEncoder.encode(self.notiEnabled) {
                                              let defaults = UserDefaults.standard
                                              defaults.set(saveData, forKey: "SYNCuserNoti")
                                          } else {
                                              print("Failed TO Save userNoti")
                                          }
                               
                               } else {
                                   DispatchQueue.main.async {
                                      self.notiEnabled = userNotifications(enabled: false)
                                   let jsonEncoder = JSONEncoder()
                                                                     if let saveData = try? jsonEncoder.encode(self.notiEnabled) {
                                                                                let defaults = UserDefaults.standard
                                                                                defaults.set(saveData, forKey: "SYNCuserNoti")
                                                                            } else {
                                                                                print("Failed TO Save userNoti")
                                                                            }
                                    cell.switchButton.setOn(false, animated: true)
                                   let ac = UIAlertController(title: "You have denied us access to send you notifications", message: "To change this Go to Settings -> Smart Scheduler -> Notifications", preferredStyle: .alert)
                                   ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                                   self.present(ac, animated: true)
                                   }
                               }
                           }
                       } else {
                    
                    self.notiEnabled = userNotifications(enabled: false)
                  if self.publicDyn {
                      self.ac(title: "Issue", message: "Dynamic Scheduling needs to be turned off before public notifications can be turned off.", button: "OK")
                    cell.switchButton.setOn(true, animated: true)
                      return
                  }
                    
                    
                           let jsonEncoder = JSONEncoder()
                           if let saveData = try? jsonEncoder.encode(self.notiEnabled) {
                                      let defaults = UserDefaults.standard
                                      defaults.set(saveData, forKey: "SYNCuserNoti")
                                  } else {
                                      print("Failed TO Save userNoti")
                                  }
                 
                    
                    
                       }
            case 1:
                if self.notiEnabled.enabled {

                    if (sender as AnyObject).isOn {
                        self.publicDyn = true
                    let jsonEncoder = JSONEncoder()
                            self.dynEnabled = saveDynScheduling(enabled: true)
                        if let saveData = try? jsonEncoder.encode(self.dynEnabled) {
                                        let defaults = UserDefaults.standard
                                                defaults.set(saveData, forKey: "SYNCdynSchedule")
                                                    } else {
                                                        print("Failed TO Save dynschedule")
                        }
                    } else {
                        self.publicDyn = false
                         let jsonEncoder = JSONEncoder()
                         self.dynEnabled = saveDynScheduling(enabled: false)
                        if let saveData = try? jsonEncoder.encode(self.dynEnabled) {
                                                    let defaults = UserDefaults.standard
                                                defaults.set(saveData, forKey: "SYNCdynSchedule")
                                                    } else {
                                                        print("Failed TO Save dynschedule")
                                               }
                    }
                } else {
                  self.ac(title: "Push Notifications needs to be enabled for Dynamic Scheduling", message: "", button: "OK")
                    cell.switchButton.isOn = false
                    let jsonEncoder = JSONEncoder()
                                           if let saveData = try? jsonEncoder.encode(false) {
                                                                       let defaults = UserDefaults.standard
                                                                   defaults.set(saveData, forKey: "SYNCdynSchedule")
                                                                       } else {
                                                                           print("Failed TO Save dynschedule")
                                                                  }
                }
            case 2:
                if (sender as AnyObject).isOn {

                    self.icloudEnabled = saveICloudSync(enabled: true)
                                                let jsonEncoder = JSONEncoder()
                    if let saveData = try? jsonEncoder.encode(self.icloudEnabled) {
                                        let defaults = UserDefaults.standard
                                                    defaults.set(saveData, forKey: "SYNCiCloud")
                                                        } else {
                                                            print("Failed TO Save SYNCiCloud")
                                                        }
                                             
                                     } else {
                                 
                                         let jsonEncoder = JSONEncoder()
                       self.icloudEnabled = saveICloudSync(enabled: false)
                    if let saveData = try? jsonEncoder.encode(self.icloudEnabled) {
                                                    let defaults = UserDefaults.standard
                                                    defaults.set(saveData, forKey: "SYNCiCloud")
                                                } else {
                                                    print("Failed TO Save SYNCiCloud")
                                                }

                                     }
            
                
            default:
                self.ac(title: "Error", message: "Error loading switch.", button: "OK")
            }
            
        }
       
        switch indexPath.row {
        case 0:
             let defaults = UserDefaults.standard
             if notiEnabled.enabled {
                 cell.switchButton.setOn(true, animated: false)
             } else {
                cell.switchButton.setOn(false, animated: false)
             }
            
            
        case 1:
              if publicDyn {
                 cell.switchButton.setOn(true, animated: false)
              
                } else {
                 cell.switchButton.setOn(false, animated: false)
                
                   }
        case 2:
            
             let defaults = UserDefaults.standard
            if let savedSet = defaults.object(forKey: "SYNCiCloud") as? Data {
                          do {
                              let jsonDecoder = JSONDecoder()
                            icloudEnabled = try jsonDecoder.decode(saveICloudSync.self, from: savedSet)
                            
                            if icloudEnabled.enabled {
                            cell.switchButton.setOn(true, animated: false)
                            iCloudSync = true
                           } else {
                            cell.switchButton.setOn(false, animated: false)
                            iCloudSync = false
                              }
                          } catch {
                              print("Failed To load icloud")
                          }
            } else {
                cell.switchButton.setOn(false, animated: false)
                                          iCloudSync = false
            }
        default:
            self.ac(title: "Error", message: "Contact Us", button: "OK")
            
        }
        return cell
    }
    override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
        print("DisAppear")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Reachability.isConnectedToNetwork(){
           generateWeak.isEnabled = true
        }else{
            ac(title: "No Internet Available", message: "Can't Create Schedules", button: "OK")
            generateWeak.isEnabled = false
        }
        print("Appear")
        checkChecks()
    }
    func checkChecks() {
        loadJSON()
        if schoolHours.count > 0 {
            checkSchoolHours = true
        } else {
            checkSchoolHours = false
        }
        if bedTime.count > 0 {
            checkBedTime = true
        } else {
            checkBedTime = false
        }
        if classesList.count > 0 {
            checkClass = true
        } else {
            checkClass = false
        }
        checkCal = false
        if userCalendarArray.count > 0 {
        let userCalendarInput = userCalendarArray[0].userCalTitle
       
        let calendars = eventStore.calendars(for: .event)
            
        for cal in calendars {
            if cal.title == userCalendarInput {
                checkCal = true
                
            }
            
        }
        }
        
       // checkTable.reloadData()
       
    }
    
    func createSchedule() {
        if !checkBedTime || !checkSchoolHours || !checkClass || !checkCal {
            ac(title: "Whoops!", message: "Not all of the required things are done. Please make sure you have school hours, a bed time, at least one class for homework, and make sure you have entered a Calendar with 'Calendar To Write To'.", button: "Ok")
            return
        }
         whileCounter229 = 0
        whileCounter260 = 0
         whileCounter1016 = 0
         whileCounter1039 = 0
         whileCounter1070 = 0
      generateDateTomorrow()
        generateDateToday()
      loadJSON()
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        generateCalendarToWrite()
        if !makeViaNoti {
          classesListForEvents = classesList
        }
        deleteOldEvents()
        print("writetobasics begins")
       writeToBasics()
        while writeToBasicsDone == false {
            print("while 229")
            whileCounter229 += 1
        }
        writeToBasicsDone = false
        print("basics are done")
        if eventIsBeforeSchoolHours {
            deleteOldEvents()
            eventIsBeforeSchoolHours = false
            ac(title: "Issue", message: "A Set Free Time Event or Extra Curricular event is before the end of School Hours. Please Fix this Issue", button: "OK")
            return
        }
        doubleActivites = false
           
        if checkDoubleActivites() {
            deleteOldEvents()
            eventIsBeforeSchoolHours = false
            ac(title: "Issue", message: "\(doubleActivitesBetweenTitle) is starting during \(doubleActivitesRangeTitle)", button: "OK")
            return
        }
        if interstitial.isReady {
                  interstitial.present(fromRootViewController: self)
                   whileAdShowing = true
                } else {
                  print("Ad wasn't ready")
                }
         algorthim()
        
        startDateOfThingsAfterSchool.removeAll()
        endDateOfThingsAfterSchool.removeAll()
        arrayOfClassesTooMuch.removeAll()
      IACounter = 0
        
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func algorthim() {
        //1. We need to Find the amount of minutes open From School hours to bed Time DONE
        //1b - classes if you can do them in study halls
        //2. We need to see how many minutes of homework + Chunked free time there is AND CLEAR CLASS BY STUDY HALL
        //3. If theres not enough time, find the approx ratio of Chunked Free Time in minutes to a chunk,
        //4.Then remove a certain number of minutes and chunks in Chunked Free Time And classses. CHECK IT
        //5. Put alternating homework then free time if possible
        
        //1. Date Components
        print("algorthim begins")
        while !schoolSchedulingDone || !bedTimeNightSchedulingDone {
            print("while 260")
            whileCounter260 += 1
        }
        schoolSchedulingDone = false
        bedTimeNightSchedulingDone = false
        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
        
        
        
        var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
        if generateAfteroon == false {
            dateComponents.year = self.yearTommorrow
            dateComponents.month = self.monthTommorrow
            dateComponents.day = self.dayTomorrow
        } else {
            dateComponents.year = self.yearToday
            dateComponents.month = self.monthToday
            dateComponents.day = self.dayToday
        }
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0 //second stays the same
        
        var dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
        
        if generateAfteroon == false {
            dateComponents2.year = self.yearTommorrow
            dateComponents2.month = self.monthTommorrow
            dateComponents2.day = self.dayTomorrow
        } else {
            dateComponents2.year = self.yearToday
            dateComponents2.month = self.monthToday
            dateComponents2.day = self.dayToday
        }
        dateComponents2.hour = 24
        dateComponents2.minute = 59
        dateComponents2.second = 0 //Second stays the same
        
        
        let startDate=Calendar.current.date(from: dateComponents)!
        let endDate=Calendar.current.date(from: dateComponents2)!
        let predicate2 = eventStore.predicateForEvents(withStart: startDate, end: endDate, calendars: nil)
        
        
        let eV = eventStore.events(matching: predicate2) as [EKEvent]
        var minutesOfExtraCurs = 0.0
        var minutesOfSetFreeTime = 0.0
        var minutesOfStudyHalls = 0
        //1. Reading and giving variables to times
            var schoolHourEvent = Date()
            var bedTimeEvent = Date()
            for i in eV {
            
                if i.title == "School hours" {
                    schoolHourEvent = i.endDate
                    endOfSchoolHours = i.endDate
                }
                if i.title == "Bed Time Night" {
                    bedTimeEvent = i.startDate
                    startOfBedTime = i.startDate
                }
                if i.title == "Set Free Time" {
                    let interval = CFDateGetTimeIntervalSinceDate(i.startDate as CFDate, i.endDate as CFDate)
                    minutesOfSetFreeTime += (interval * -1) / 60
                    if FTPO {
                        if i.startDate.changeDateToDateToday() > Date() {
                                             startDateOfThingsAfterSchool.append(i.startDate)
                                             endDateOfThingsAfterSchool.append(i.endDate)
                        }
                    } else {
                        startDateOfThingsAfterSchool.append(i.startDate)
                                           endDateOfThingsAfterSchool.append(i.endDate)
                    }
                 
                   
                }
                if i.title == "Study Hall" {
                    let interval = CFDateGetTimeIntervalSinceDate(i.startDate as CFDate, i.endDate as CFDate)
                    minutesOfStudyHalls += Int((interval * -1) / 60)
                }
                for e in 0..<eventsToDelete.count {
                    if eventsToDelete[e].name.contains(i.title) {
                        let interval = CFDateGetTimeIntervalSinceDate(i.startDate as CFDate, i.endDate as CFDate)
                        minutesOfExtraCurs += (interval * -1) / 60
                        if FTPO {
                            if i.startDate.changeDateToDateToday() > Date() {
                                                                    startDateOfThingsAfterSchool.append(i.startDate)
                                                                    endDateOfThingsAfterSchool.append(i.endDate)
                                               }
                                           } else {
                                               startDateOfThingsAfterSchool.append(i.startDate)
                                                                  endDateOfThingsAfterSchool.append(i.endDate)
                                           }
                       
                    }
                    
                }
//                if debugOn {
//                    if generateAfteroon {
//                        schoolHourEvent = schoolHours[0].endTime.changeDateToDateToday()
//
//                        bedTimeEvent = bedTime[0].startTime.changeDateToDateToday()
//                    } else {
//                           bedTimeEvent = bedTime[0].startTime.changeDateToDateTommorow()
//                         schoolHourEvent = schoolHours[0].endTime.changeDateToDateTommorow()
//                    }
//
//                }
                //if events to delte.name contains i.title......
            //record it
                
            }
       //1. Calculating Minutes of Remaing Time
        print("break")
        var minutes = 0.0
        if FTPO  {
            minutes = CFDateGetTimeIntervalSinceDate(Date() as CFDate, testBedStart as CFDate)
        } else {
             minutes = CFDateGetTimeIntervalSinceDate(schoolHourEndDate as CFDate, testBedStart as CFDate)
        }
//
         var minutesBetweenSchoolAndBedTime = ((minutes * -1) / 60).rounded(.down)
        if minutes > 0 {
            minutesBetweenSchoolAndBedTime *= -1
        }
                   
      
       
        
        let minutesOfRemaingTime = Int(minutesBetweenSchoolAndBedTime - minutesOfExtraCurs - minutesOfSetFreeTime)
        print("Minutes of Remaining: \(minutesOfRemaingTime). Minutes Between School and BedTime: \(minutesBetweenSchoolAndBedTime) - Minutes of Extra Curs: \(minutesOfExtraCurs) - Minutes of Set Free Time: \(minutesOfSetFreeTime)")
        var minutesOfClass = 0
        
        
                                                                     
        
        if classesListForEvents.count > 0 {
            classesListForEvents.sort(by: { $0.priority! > $1.priority! })
           
        }
        //1b
        //STUDY HALL CLASES BEING CUT LN 343
        //CUTTING CLASSES IN STUDY HALL
//        if generateAfteroon == false {
//        for hw in classesListForEvents {
//
//            if minutesOfStudyHalls > Int(hw.time!)! {
//
//                minutesOfStudyHalls -= Int(hw.time!)!
//
//                minutesOfClassInStudyHall += Int(hw.time!)!
//                classesInStudyHalls.append(StudyHallClass(name: hw.name!, length: Int(hw.time!)!))
//                hw.time = "0"
//                print("Study Hall Removed \(hw.name!)")
//
//            } else {
//                var numb = Int(hw.time!)!
//                numb -= minutesOfStudyHalls
//                 minutesOfStudyHalls -= Int(hw.time!)!
//                 minutesOfClassInStudyHall += minutesOfStudyHalls
//                hw.time = String(numb)
//                classesInStudyHalls.append(StudyHallClass(name: hw.name!, length: numb))
//                print("Study Hall Partialy Remove \(hw.name!) New time: \(hw.time!)")
//             ///   if minutesOfStudyHalls < 0 {
//                    break
//              //  }
//
//            }
//
//        }
//        print("Minutes Of Class In Study Hall = \(minutesOfClassInStudyHall)")
//        }
        //2. Reading MOC and MOCRT
        if classesListForEvents.count > 0 {
        for hw in classesListForEvents {
            minutesOfClass += Int(hw.time!)!
            
            }
        }
        if !makeViaNoti {
        if relaxTime.count > 0 {
            minutesOfChunkedRelaxTime += relaxTime[0].time
        }
        }
      //2. Added MOC and MOCRT to get MOCACRT
        let minutesOfClassesAndChunkedRelaxTime = minutesOfClass + minutesOfChunkedRelaxTime
        print("Minutes of Classes AND chunked Relax Time: \(minutesOfClassesAndChunkedRelaxTime) - Minute ofl Class: \(minutesOfClass) - Minutes of chunked Relax Time \(minutesOfChunkedRelaxTime)")
    
                var ratioOfSessionsToTime = 0
        //3/4. Cut Relax Time Or Classes
       
        if minutesOfClassesAndChunkedRelaxTime > minutesOfRemaingTime {
            let minutesOverLimit = minutesOfClassesAndChunkedRelaxTime - minutesOfRemaingTime
            print("MOL - \(minutesOverLimit)")
            if minutesOfClass > minutesOfRemaingTime {
                print("Start Cutting Classes Immeditaly")
                alertLine1 = "We had to cut all of the Chunked Free Time"
               
                //4. Start immeditaly cutting classes
                numberOfSessions = 0
                //ERROR MOL IS NOT BEING AFFECTED by MOL - MOCRT
                ratioOfSessionsToTime = 0
                var newMinutesOverLimit = minutesOverLimit - minutesOfChunkedRelaxTime
                minutesOfChunkedRelaxTime = 0
                var classesBeingFullyCut = [String]()
                print("NEW Minutes over limit - \(newMinutesOverLimit)")
                
                for hw in classesListForEvents.reversed() {
                    print("Homework to cut \(hw.name!) Priorty \(hw.priority!)")
                    if newMinutesOverLimit > Int(hw.time!)! {
                       
                        newMinutesOverLimit -= Int(hw.time!)!
                        
                         hw.time = "0"
                        classesBeingFullyCut.append(hw.name!)
                        
                    } else {
                        var numb = Int(hw.time!)!
                        numb -= newMinutesOverLimit
                        hw.time = String(numb)
                       alertLine2 = "We had to partially cut out \(hw.name!)"
                        break
                    }
                    
                    
                }
                var line3Str = "We had to fully cut out: "
                for hw2 in classesBeingFullyCut {
                    line3Str += "\(hw2), "
                    
                }
                alertLine3 = line3Str
                for hw in classesListForEvents {
                 //   print("New HW \(hw.name) - \(hw.time)")
                    if Int(hw.time!)! <= 0 {
                        //WE MUST REMOVE IT
                    }
                }
            }
            if minutesOfChunkedRelaxTime > 0 {
                 print("Start Cutting Relax Time Immeditaly")
                //3. Getting Ratio - FIX IT
                alertLine1 = "We had to cut some Chunked Free Time"
                alertLine2 = "We had to remove \(minutesOverLimit) minutes"
               
                if relaxTime[0].sessions != "Any Number" {
                    print("Session != Any")
                   
                    ratioOfSessionsToTime = relaxTime[0].time / Int(relaxTime[0].sessions)!
                    
                   minutesOfChunkedRelaxTime -= minutesOverLimit
                    //minutes / rostt for sessions
                   numberOfSessions = minutesOfChunkedRelaxTime / ratioOfSessionsToTime
                    
                    if numberOfSessions == 0 { numberOfSessions = 1}
                    print("Ratio of Sessions to Time: \(ratioOfSessionsToTime) - NEW Minutes of chunked Relax Time: \(minutesOfChunkedRelaxTime) - Number of Sessions: \(numberOfSessions)")
                     alertLine3 = "Number of Sessions went from \(oldNumbSessions) to \(numberOfSessions)"
                } else {
                
                    print("Session = Any")
                     minutesOfChunkedRelaxTime -= minutesOverLimit
                     numberOfSessions = classesList.count
                    print("NEW Minutes of chunked Relax Time: \(minutesOfChunkedRelaxTime) - Number of Sessions: \(numberOfSessions)")
                }
                
            }
            
           
        } else {
            if relaxTime[0].sessions != "Any Number" {
                print("Session != Any")
               
                numberOfSessions = Int(relaxTime[0].sessions)!
                if numberOfSessions == 0 { numberOfSessions = 1}
                print("Ratio of Sessions to Time: \(ratioOfSessionsToTime) - SAME Minutes of chunked Relax Time: \(minutesOfChunkedRelaxTime) - Number of Sessions: \(numberOfSessions)")
            } else {
                print("Session = Any")
               
                numberOfSessions = classesList.count
                print("NEW Minutes of chunked Relax Time: \(minutesOfChunkedRelaxTime) - Number of Sessions: \(numberOfSessions)")
            }
        }
       
        //5. Make Events
        print("writeAlgorthimGenereatedEvents")
     writeAlgorthimGenereatedEvents()
        
        
    
       
        
        
    
    }
    func loadJSON() {
        print("Load JSON")
        let defaults = UserDefaults.standard

        if let savedSet = defaults.object(forKey: "SYNCuserCal") as? Data {
            do {
                let jsonDecoder = JSONDecoder()
                userCalendarArray = try jsonDecoder.decode([userCal].self, from: savedSet)
                calendarButtonText.setTitle("Calendar To Write To: \(userCalendarArray[0].userCalTitle)", for: .normal)
            } catch {
                print("Failed To load userCal")
            }
        }
        if let savedSet = defaults.object(forKey: "SYNCuserNoti") as? Data {
            do {
                let jsonDecoder = JSONDecoder()
             notiEnabled = try jsonDecoder.decode(userNotifications.self, from: savedSet)
            } catch {
                print("erroR")
            }
               
        } else {
            notiEnabled = userNotifications(enabled: false)
        }
     
                           if let savedSet = defaults.object(forKey: "SYNCdynSchedule") as? Data {
                                         do {
                                             let jsonDecoder = JSONDecoder()
                                            let test2 = try jsonDecoder.decode(saveDynScheduling.self, from: savedSet)
                                            publicDyn = test2.enabled
                                          
                                         } catch {
                                             print("Failed To load SYNCdynSchedule")
                                         }
                           } else {
                            publicDyn = false
        }
                   if let savedSet = defaults.object(forKey: "SYNCiCloud") as? Data {
                                 do {
                                     let jsonDecoder = JSONDecoder()
                                    icloudEnabled = try jsonDecoder.decode(saveICloudSync.self, from: savedSet)
                                    if icloudEnabled.enabled {
                                 
                                   iCloudSync = true
                                  } else {
                                  
                                   iCloudSync = false
                                     }
                                 } catch {
                                     print("Failed To load iCloudSYNC")
                                 }
                   } else {
                    iCloudSync = false
        }
        if let savedSet = defaults.object(forKey: "SYNCchunkedRelaxTime") as? Data {
            do {
                let jsonDecoder = JSONDecoder()
                relaxTime = try jsonDecoder.decode([RelaxTime].self, from: savedSet)
               
              
            oldNumbSessions = relaxTime[0].sessions
            } catch {
                print("Failed To load chunkedRelaxTime")
            }
        } else {
            relaxTime.removeAll()
             relaxTime.append(RelaxTime(time: 0, sessions: "Any Number"))
        }
       
        if let savedSet = defaults.object(forKey: "SYNCsetRelaxTimes") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                setRelaxTimes = try jsonDecoder.decode([RelaxSetTime].self, from: savedSet)
                
            } catch {
                print("Failed To load setRelaxTimes")
            }
        }
        
//        if let savedHall = defaults.object(forKey: "SYNCstudyHalls") as? Data {
//            let jsonDecoder = JSONDecoder()
//            do {
//
//                studyHallList = try jsonDecoder.decode([StudyHall].self, from: savedHall)
//                
//
//            } catch {
//                print("Failed To load Study Halls")
//            }
//        }
        
            if let savedClass = defaults.object(forKey: "SYNCschoolHours") as? Data {
                
                do {
                    let jsonDecoder = JSONDecoder()
                    schoolHours = try jsonDecoder.decode([SchoolHour].self, from: savedClass)
                    stopAccessToEverythingButSchoolHours = false
                    
                } catch {
                   
                    print("Failed To load schoolHours")
                }
            } else {
                 stopAccessToEverythingButSchoolHours = true
        }
        
            if let savedClass = defaults.object(forKey: "SYNChomework") as? Data {
                let jsonDecoder = JSONDecoder()
                do {
                    classesList = try jsonDecoder.decode([Classes].self, from: savedClass)
                    if !makeViaNoti {
                    classesListForEvents = classesList
                    }
                    
                } catch {
                    print("Failed To load homework")
                }
            }
        
            if let savedPeople = defaults.object(forKey: "SYNCextraCur") as? Data {
                let jsonDecoder = JSONDecoder()
                do {
                    extraCurList = try jsonDecoder.decode([ExtraCur].self, from: savedPeople)
                    
                } catch {
                    print("Failed To load ExtraCur")
                }
                
            }
        
        if let savedBTime = defaults.object(forKey: "SYNCbedTime") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                bedTime = try jsonDecoder.decode([BedTime].self, from: savedBTime)
                stopAccessToEverythingButBedTime = false
            } catch {
                 
                print("Failed To load BedTime")
            }
            
        } else {
            stopAccessToEverythingButBedTime = true
        }
        
        
        if let savedPeople = defaults.object(forKey: "SYNCevents") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                eventsToDelete = try jsonDecoder.decode([EventToDelete].self, from: savedPeople)
               
            } catch {
                print("Old Events")
            }
            
        }
        
        
            print("Writing")
        
    }
    
    func writeToBasics() {
        eventStore.requestAccess(to: EKEntityType.event, completion:
            {(granted, error) in
                if !granted {
                    self.ac(title: "Error", message: "We do not have access to your calendar. Please enable it in settings. More information can be found in our Privacy Policy.", button: "OK")
                    return
                }
                //Mark: Write to calendar
               
               
               //delete old events
                if self.generateAfteroon == false {
                    
                    //     self.eventSetStudyHalls()
                }
              self.eventSchoolHours()
                
                self.eventExtraCurs()
                self.eventSetRelaxTimes()
               
                self.eventBedTime()
               
                self.writeToBasicsDone = true
                
        })
    
     
        
        
    }
    func deleteOldEvents() {
         let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
        
        
        
         var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
        if generateAfteroon == false {
        dateComponents.year = self.yearTommorrow
        dateComponents.month = self.monthTommorrow
        dateComponents.day = self.dayTomorrow
        } else {
            dateComponents.year = self.yearToday
            dateComponents.month = self.monthToday
            dateComponents.day = self.dayToday
        }
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0 //second stays the same
        
        var dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
        
        if generateAfteroon == false {
        dateComponents2.year = self.yearTommorrow
        dateComponents2.month = self.monthTommorrow
        dateComponents2.day = self.dayTomorrow
        } else {
            dateComponents2.year = self.yearToday
            dateComponents2.month = self.monthToday
            dateComponents2.day = self.dayToday
        }
        dateComponents2.hour = 24
        dateComponents2.minute = 59
        dateComponents2.second = 0 //Second stays the same
        
        
        let startDate=Calendar.current.date(from: dateComponents)!
        let endDate=Calendar.current.date(from: dateComponents2)!
        let predicate2 = eventStore.predicateForEvents(withStart: startDate, end: endDate, calendars: nil)
        
        
        let eV = eventStore.events(matching: predicate2) as [EKEvent]
        
      
            for i in eV {
                print("for 739")
                if i.title == "School hours" || i.title == "Set Free Time" || i.title == "Study Hall" || i.title == "Bed Time Night" || i.title == "Bed Time Morning" {
                //    print("First parameter \(i.title ?? "Major error")")
                    do{
                        (try eventStore.remove(i, span: EKSpan.thisEvent, commit: true))
                    }
                    catch  {
                        assert(true, "Error Failed To delete")
                    }
                }
                if i.calendar == userCalendar {
                    do{
                                            (try eventStore.remove(i, span: EKSpan.thisEvent, commit: true))
                                        }
                                        catch  {
                            assert(true, "Error Failed To delete")
                            }
                }

                for event in eventsToDelete {
                    if event.name == i.title {
                        print("eventsToDelete Items \(i.title ?? "Major Error")")
                        do{
                            (try eventStore.remove(i, span: EKSpan.thisEvent, commit: true))
                        }
                        catch  {
                            assert(true, "Error Failed To delete")
                        }
                    }
                }

            }
        
         eventsToDelete.removeAll()
    }
    
    func eventSchoolHours() {
       
        
        let event = EKEvent(eventStore: self.eventStore)
        event.title = "School hours"
         event.alarms = []
        if generateAfteroon {
             
        
            event.startDate = schoolHours[0].startTime.changeDateToDateToday()
            event.endDate = schoolHours[0].endTime.changeDateToDateToday()
        } else {
           
            event.startDate = schoolHours[0].startTime.changeDateToDateTommorow()
            event.endDate = schoolHours[0].endTime.changeDateToDateTommorow()
        }
       
      
       
        schoolHourEndDate = event.endDate
      //  event.calendar = self.eventStore.defaultCalendarForNewEvents
    event.calendar = userCalendar
        do {
            try self.eventStore.save(event, span: .thisEvent)
            alertLineTestStr += " School hour Event SUCCESS"
        schoolSchedulingDone = true
        } catch let error {
            ac(title: "Error", message: "School Hour Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")
           
        }
        //return
    }
    func eventExtraCurs() {
        if extraCurList.count == 0 {

        } else {
        for cur in 0..<extraCurList.count {
       print("811")
            if FTPO {
                if extraCurList[cur].startTime.changeDateToDateToday() < Date() {
                return
            }
            }
        let event = EKEvent(eventStore: self.eventStore)
        event.title = extraCurList[cur].name
             event.alarms = []
            if generateAfteroon {
                event.startDate = extraCurList[cur].startTime.changeDateToDateToday()
                event.endDate = extraCurList[cur].endTime.changeDateToDateToday()
            } else {
                event.startDate = extraCurList[cur].startTime.changeDateToDateTommorow()
                event.endDate = extraCurList[cur].endTime.changeDateToDateTommorow()
            }
         eventsToDelete.append(EventToDelete(name: event.title!))
            saveEventToDelete()
      //  event.calendar = self.eventStore.defaultCalendarForNewEvents
        event.calendar = userCalendar

            if event.startDate < schoolHourEndDate {
                eventIsBeforeSchoolHours = true
            }

        do {
            try self.eventStore.save(event, span: .thisEvent)

        } catch let error {
             ac(title: "Error", message: "Extra Cur Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")
        }




        }
        }
    }
    func eventSetRelaxTimes() {
        if setRelaxTimes.count == 0 {

        } else {
            for set in 0..<setRelaxTimes.count {
              print("for 851")
                if FTPO {
                    if setRelaxTimes[set].startTime.changeDateToDateToday() < Date() {
                             return
                         }
                }
                let event = EKEvent(eventStore: self.eventStore)
                event.title = "Set Free Time"
                 event.alarms = []

            

                if generateAfteroon {
                    event.startDate = setRelaxTimes[set].startTime.changeDateToDateToday()
                    event.endDate = setRelaxTimes[set].endTime.changeDateToDateToday()
                } else {
                     event.startDate = setRelaxTimes[set].startTime.changeDateToDateTommorow()
                        event.endDate = setRelaxTimes[set].endTime.changeDateToDateTommorow()
                }
             
               // event.calendar = self.eventStore.defaultCalendarForNewEvents
                 event.calendar = userCalendar
                if event.startDate < schoolHourEndDate {
                               eventIsBeforeSchoolHours = true
                           }
                do {
                    try self.eventStore.save(event, span: .thisEvent)

                } catch let error {
                     ac(title: "Error", message: "Set Free Time Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")
                }
            }
        }
    }
    func eventSetStudyHalls() {
        if studyHallList.count == 0 {

        } else {
            for hall in 0..<studyHallList.count {
              print("for 887")
                let event = EKEvent(eventStore: self.eventStore)
                event.title = "Study Hall"
                 event.alarms = []

                if generateAfteroon {
                    event.startDate = studyHallList[hall].startTime.changeDateToDateToday()
                    event.endDate = studyHallList[hall].endTime.changeDateToDateToday()

                    
                } else {
                    event.startDate = studyHallList[hall].startTime.changeDateToDateTommorow()
                   event.endDate = studyHallList[hall].endTime.changeDateToDateTommorow()
                }
               // event.calendar = self.eventStore.defaultCalendarForNewEvents
                  event.calendar = userCalendar
                do {
                    try self.eventStore.save(event, span: .thisEvent)

                } catch let error {
                    ac(title: "Error", message: "Study Hall Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")

                }
            }
        }
    }
    func eventBedTime() {
        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]

        var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())



        //AM
        let event = EKEvent(eventStore: self.eventStore)
        event.title = "Bed Time Morning"

         event.alarms = []
        if generateAfteroon == false {
            dateComponents.year = self.yearTommorrow
            dateComponents.month = self.monthTommorrow
            dateComponents.day = self.dayTomorrow
             event.endDate = bedTime[0].endTime.changeDateToDateTommorow()
        } else {
            dateComponents.year = self.yearToday
            dateComponents.month = self.monthToday
            dateComponents.day = self.dayToday
             event.endDate = bedTime[0].endTime.changeDateToDateToday()
        }
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0 //second stays the same
        
        event.startDate = Calendar.current.date(from: dateComponents)!
       
        //event.calendar = self.eventStore.defaultCalendarForNewEvents
         event.calendar = userCalendar
        do {
            if generateAfteroon == false {
            try self.eventStore.save(event, span: .thisEvent)
                
            }
           

        } catch let error {
            ac(title: "Error", message: "Bed Time Morning Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")

        }

        //PM
        let event2 = EKEvent(eventStore: self.eventStore)
        event2.title = "Bed Time Night"
       
               
        
       var dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
           
        if generateAfteroon == false {
            dateComponents2.year = self.yearTommorrow
            dateComponents2.month = self.monthTommorrow
            dateComponents2.day = self.dayTomorrow
            event2.startDate = bedTime[0].startTime.changeDateToDateTommorow()
        } else {
            dateComponents2.year = self.yearToday
            dateComponents2.month = self.monthToday
            dateComponents2.day = self.dayToday
            event2.startDate = bedTime[0].startTime.changeDateToDateToday()
        }
        dateComponents2.hour = 24
        dateComponents2.minute = 0
        dateComponents2.second = 0
        event2.endDate = Calendar.current.date(from: dateComponents2)!
        testBedStart = event2.startDate
        event2.calendar = userCalendar


        do {
            try self.eventStore.save(event2, span: .thisEvent)
           
            bedTimeNightSchedulingDone = true
            print("bed night schedueld")

        } catch let error {
            ac(title: "Error", message: "Bed Time Night Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")
             

        }






        
        
        
        
        
    }
    func writeAlgorthimGenereatedEvents() {
        //4 important Vars
        //WE NEED TO SEE THE STUDYHALL WE DELETED
        //classesListForEvents
        //minutesOfClassInStudyHall
        //MinutesOfChunkedRelaxTime
        //NumberOFSessions
        
       
        endDateOfThingsAfterSchool.insert(endOfSchoolHours, at: 0)
        startDateOfThingsAfterSchool.append(startOfBedTime)
        if FTPO {
            
            let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]




                   //AM
                   let event = EKEvent(eventStore: self.eventStore)
                   event.title = "Blocker for Generate From Right Now"

                    event.alarms = []
                
                   
            event.startDate = schoolHours[0].endTime.changeDateToDateToday()
            event.endDate = Date()
           
                   //event.calendar = self.eventStore.defaultCalendarForNewEvents
                    event.calendar = userCalendar
                                        
                     //  try self.eventStore.save(event, span: .thisEvent)
                           
                    endDateOfThingsAfterSchool.insert(event.endDate, at: 0)
                    startDateOfThingsAfterSchool.insert(event.startDate, at: 0)
                    eventsToDelete.append(EventToDelete(name: event.title))

                

        }
        while classesListForEvents.count != 0 {
            print("while 1016")
            whileCounter1016 += 1
           let i = 0
          
            
            if classesListForEvents.count == 1 {
                print("")
                
            }
       startDateOfThingsAfterSchool.sort(by: { $0 < $1 })
        endDateOfThingsAfterSchool.sort(by: { $0 < $1 })
            if Double(classesListForEvents[i].time!)! <= 0.0 {
                classesListForEvents.remove(at: i)
                continue
            }
            let timeBetween = CFDateGetTimeIntervalSinceDate(endDateOfThingsAfterSchool[i] as CFDate, startDateOfThingsAfterSchool[i] as CFDate)
            
            
                var minutesBetween = timeBetween / 60
                if minutesBetween < 0 {
                    minutesBetween *= -1
                }
                scheduleClassOrRelaxTime(minutesBetween: minutesBetween, startingDate: startDateOfThingsAfterSchool[i], endingDate: endDateOfThingsAfterSchool[i], i: i, isClass: true)
                
            while doneWithFunction == false {
                print("While 1039")
                whileCounter1039 += 1
            }
            doneWithFunction = false
            if !plantedSuccessfully {
               
                startDateOfThingsAfterSchool.remove(at: i)
                endDateOfThingsAfterSchool.remove(at: i)
            }
            if plantedSuccessfully {
                if numberOfSessions > 0 {
                    
                    
                    addChunkedRelaxTime()
                    
                    
                    
                    
                }
            }
            
            plantedSuccessfully = false
            
            
            
            
            }
        if numberOfSessions > 0 {
            //TAKE THE CHUNKED, AND USE IT UP
            var deletedEndDateOfThingsAfterSchool = [Date]()
            var deletedStartDateOfThingsAfterSchool = [Date]()
            while numberOfSessions > 0 {
                print("while 1070")
                whileCounter1070 += 1
                startDateOfThingsAfterSchool.sort(by: { $0 < $1 })
                endDateOfThingsAfterSchool.sort(by: { $0 < $1 })
                let i = 0
                if !endDateOfThingsAfterSchool.isEmpty || !startDateOfThingsAfterSchool.isEmpty {
               let timeBetween = CFDateGetTimeIntervalSinceDate(endDateOfThingsAfterSchool[i] as CFDate, startDateOfThingsAfterSchool[i] as CFDate)
                
                var minutesBetween = timeBetween / 60
                if minutesBetween < 0 {
                    minutesBetween *= -1
                }
                freeTimeTime = minutesOfChunkedRelaxTime / numberOfSessions
       //         print("FREE TIME \(freeTimeTime)")
                //PROBLEM 2A
                //FOR SOME REASON, ITS THE CLASS TIME THAT IS MINUTES BETWEEN??????
                if Int(minutesBetween) >= freeTimeTime {
                    minutesOfChunkedRelaxTime -= freeTimeTime

                    //KEEP IN MIND MinutesofChunkedRelax COULD BE CUT BY THE MOL
                    scheduleClassOrRelaxTime(minutesBetween: minutesBetween, startingDate: startDateOfThingsAfterSchool[i], endingDate: endDateOfThingsAfterSchool[i], i: i, isClass: false)
                    numberOfSessions -= 1
                     if startDateOfThingsAfterSchool[i]  == startOfBedTime {
                        break

                    }
                    
                } else {
                    deletedEndDateOfThingsAfterSchool.insert(endDateOfThingsAfterSchool[i], at: 0)
                    deletedStartDateOfThingsAfterSchool.insert(startDateOfThingsAfterSchool[i], at: 0)
                    endDateOfThingsAfterSchool.remove(at: 0)
                    startDateOfThingsAfterSchool.remove(at: 0)
                }
                } else {
                    endDateOfThingsAfterSchool = deletedEndDateOfThingsAfterSchool
                    startDateOfThingsAfterSchool = deletedStartDateOfThingsAfterSchool
                    startDateOfThingsAfterSchool.sort(by: { $0 < $1 })
                    endDateOfThingsAfterSchool.sort(by: { $0 < $1 })
                    numberOfSessions += 1
                    //NOTE THIS
                }
            }
            
            
            
        }
        print("BROKEN OUT OF LOOP")
      
//        if alertLine1 == "" {
//            alertLine1 = "No Alerts"
//            alertLine3 = "-"
//            alertLine2 = "-"
//            alertLineTest = "-"
//        }
//        let ac = UIAlertController(title: "Alerts", message: "\(alertLine1) \n \(alertLine2) \n \(alertLine3)", preferredStyle: .alert)
//        ac.addAction(UIAlertAction(title: "Open Calendar", style: .default) {
//            action in
//            let instagramHooks = "calshow://"
//            let instagramUrl = NSURL(string: instagramHooks)
//
//            UIApplication.shared.open(instagramUrl! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
//        })
//        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//      present(ac, animated: true)
//        alertLine1 = ""
//        alertLine3 = ""
//        alertLine2 = ""
       
        //FROM HERE WRITE STUDY HALL CALSS EVENT
   //     scheduleStudyHallClasses()
//
//        for i in 0..<startDateOfThingsAfterSchool.count {
//            if i != 0 {
//           let timeBetween = CFDateGetTimeIntervalSinceDate(endDateOfThingsAfterSchool[i - 1] as CFDate, startDateOfThingsAfterSchool[i] as CFDate)
//                let minutesBetween = (timeBetween * -1) / 60
//
//                print("Time Between: \(minutesBetween)")
//                scheduleClassOrRelaxTime(minutesBetween: minutesBetween, startingDate: endDateOfThingsAfterSchool[i - 1], endingDate: startDateOfThingsAfterSchool[i], i: i)
//            } else {
//                 let timeBetween = CFDateGetTimeIntervalSinceDate(endDateOfThingsAfterSchool[0] as CFDate, startDateOfThingsAfterSchool[0] as CFDate)
//                let minutesBetween = (timeBetween * -1) / 60
//
//                print("Time Between: \(minutesBetween)")
//                scheduleClassOrRelaxTime(minutesBetween: minutesBetween, startingDate: endDateOfThingsAfterSchool[0], endingDate: startDateOfThingsAfterSchool[0], i: i)
//            }
//
//
//        }
        //WE Can put the thing below as a else if statment aboe
        
            
        
//      let test = CFDateGetTimeIntervalSinceDate(endOfSchoolHours as CFDate, startDateOfThingsAfterSchool[1] as CFDate)
//    let test2 = (test * -1) / 60
//        print("Test2: \(test2)")
        
    }
    func addChunkedRelaxTime() {
        startDateOfThingsAfterSchool.sort(by: { $0 < $1 })
        endDateOfThingsAfterSchool.sort(by: { $0 < $1 })
        let timeBetween2 = CFDateGetTimeIntervalSinceDate(endDateOfThingsAfterSchool[IACounter] as CFDate, startDateOfThingsAfterSchool[IACounter] as CFDate)
        if endDateOfThingsAfterSchool[IACounter] == startDateOfThingsAfterSchool[IACounter] {
            print("Scheduling FREE time, END AND START SAME")
            
        }
        var minutesBetween2 = timeBetween2 / 60
        if minutesBetween2 < 0 {
            minutesBetween2 *= -1
        }
        freeTimeTime = minutesOfChunkedRelaxTime / numberOfSessions
        print("Free time \(freeTimeTime)")
        
        
        print("TESTING Int(minutesBetween) >= freeTimeTime ")
        if Int(minutesBetween2) >= freeTimeTime {
            print("SUCESSFUL")
            minutesOfChunkedRelaxTime -= freeTimeTime
            
            //KEEP IN MIND MinutesofChunkedRelax COULD BE CUT BY THE MOL
            scheduleClassOrRelaxTime(minutesBetween: minutesBetween2, startingDate: startDateOfThingsAfterSchool[IACounter], endingDate: endDateOfThingsAfterSchool[IACounter], i: IACounter, isClass: false)
            numberOfSessions -= 1
            IACounter = 0
        } else {
           IACounter += 1
            if startDateOfThingsAfterSchool.count < IACounter + 1 {
                print("Returned")
                IACounter = 0
                return
            }
            addChunkedRelaxTime()
//            print("------------------------------------------------------FAIl MINUTES BETWEEN: \(minutesBetween2). Start Date is \(startDateOfThingsAfterSchool[i]) Ending is \(endDateOfThingsAfterSchool[i]) ----------------------------------------------------")
        }
    }
    func scheduleStudyHallClasses() {
        for study in classesInStudyHalls {
            print("for 1206")
            let event = EKEvent(eventStore: self.eventStore)
            event.title = study.name
             event.alarms = []
            
          //  event.calendar = self.eventStore.defaultCalendarForNewEvents
            event.calendar = userCalendar
            eventsToDelete.append(EventToDelete(name: event.title!))
            saveEventToDelete()
            do {
                try self.eventStore.save(event, span: .thisEvent)
                
                
                
            } catch let error {
                ac(title: "Error", message: "StudyHall Class Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")
                
            }
        }
    }
    func scheduleClassOrRelaxTime(minutesBetween: Double, startingDate: Date, endingDate: Date, i: Int, isClass: Bool) {
        
        if isClass == true {
        if minutesBetween > 0 && classesListForEvents.count > 0 {
            
           //i == 0 breaks if the minutesbetween < 5. Thats why math isnt coming
                if Double(classesListForEvents[i].time!)! < minutesBetween {
                     print("Classes are too little")
                    //CALCulate end of class and then put class and then maybe put relax
                    // I NEED TO FILL THIS
                    let time = Double(classesListForEvents[i].time!)!
                    let timeInSeconds = time * 60
                //    let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                    
                    plantedSuccessfully = true
                    
                    let refrence = endingDate.timeIntervalSinceReferenceDate
                 
                    
                    let event = EKEvent(eventStore: self.eventStore)
                    event.title = classesListForEvents[i].name!
                    event.alarms = []
                    
                    event.notes = classesListForEvents[i].notes!
                    //was starting date
                    event.startDate = endingDate
                    event.endDate = Date(timeIntervalSinceReferenceDate: refrence + timeInSeconds)
                    //NEW THINGS --
                    startDateOfThingsAfterSchool.insert(event.startDate, at: 0)
                    endDateOfThingsAfterSchool.insert(event.endDate, at: 0)
                    //--
                   // event.calendar = self.eventStore.defaultCalendarForNewEvents
                    event.calendar = userCalendar
                    eventsToDelete.append(EventToDelete(name: event.title!))
                    saveEventToDelete()
                   
                    if notiEnabled.enabled {
                        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                        var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: event.startDate)
                          var dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: event.endDate)
                        
                        dateComponents.second = 0
                        dateComponents2.second = 0
                        var startAMPM = ""
                         var endAMPM = ""
                        let center = UNUserNotificationCenter.current()
                        center.delegate = self
                       
//                          let show = UNNotificationAction(identifier: "show", title: "I did not finish the previous homework on time", options: .foreground)
//                          let category = UNNotificationCategory(identifier: "alarm", actions: [show], intentIdentifiers: [])

//                          center.setNotificationCategories([category])
                           let content = UNMutableNotificationContent()
                        var startEventHour = dateComponents.hour!
                        var endEventHour = dateComponents2.hour!
                        var startEventMinute = String(dateComponents.minute!)
                        
                        if dateComponents.minute! < 10 {
                            startEventMinute = "0\(dateComponents.minute!)"
                        }
                        var endEventMinute = String(dateComponents2.minute!)
                        
                        if dateComponents2.minute! < 10 {
                            endEventMinute = "0\(dateComponents2.minute!)"
                        }
                         let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
                        if dateFormat.firstIndex(of: "a") == nil {
                                  
                                  } else {
                                      
                                           
                                
                                      if startEventHour == 12 {
                                          startAMPM = "pm"
                                      } else {
                                          if startEventHour <= 12 {
                                                             startAMPM = "am"
                                                         } else {
                                                             startEventHour -= 12
                                                             startAMPM = "pm"
                                                         }
                                      }
                                      if endEventHour == 12 {
                                          endAMPM = "pm"
                                      } else {
                                          if endEventHour <= 12 {
                                                      endAMPM = "am"
                                                        } else {
                                                            endEventHour -= 12
                                                            endAMPM = "pm"
                                                        }
                                                        
                                      }
                                      
                                    
                                   
                                  }
                        content.title = "\(event.title!) for \(Int(time)) minutes \(startEventHour):\(startEventMinute) \(startAMPM) - \(endEventHour ):\(endEventMinute) \(endAMPM)"
                        
                        content.body = "Notes: \(event.notes ?? "Error")"
                        var tempArray = classesList
                        tempArray.sort(by: { $0.priority! > $1.priority! })
                         var index2 = tempArray.firstIndex(where: { $0.name == classesListForEvents[i].name })
                        if index2! > 0 {
                        index2! -= 1
                        }
                           content.categoryIdentifier = "alarm"
                        content.userInfo = [
                            "nameOfClass": classesListForEvents[i].name!,
                            "indexBefore": index2!,
                            "classToLittle": "yes",
                            "arrayClassesTooMuch": arrayOfClassesTooMuch
                        ]
                        
                           content.sound = UNNotificationSound.default
                        
//
                     
                        
                       
                            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
                        
                        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
                        center.add(request)
                    }
                     classesListForEvents.remove(at: i)
                    do {
                        try self.eventStore.save(event, span: .thisEvent)
                        
                        
                        
                    } catch let error {
                        ac(title: "Error", message: "Class Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")
                        
                    }
                } else {
                    print("Classes are too much")
                    //Cut off class and put, NO RELAX TIME
                    let MOL = Double(classesListForEvents[i].time!)! - minutesBetween
                  let time = Double(classesListForEvents[i].time!)! - MOL
                    let timeInSeconds = time * 60
                    //time is minutes
                    
                 //   let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                 //
                    plantedSuccessfully = true
                    
                   let refrence = endingDate.timeIntervalSinceReferenceDate
//                    let seperated = bedTime[0].startTime?.split(separator: ":")
//
//                    let seperated2 = bedTime[0].endTime?.split(separator: ":")
// plantedSuccessfully = true
//                    var startHour = Int(seperated![0])!
//
//                    if bedTime[0].startAMPM == "pm" {
//
//                        startHour += 12
//                    }
//
//                    var endHour = Int(seperated2![0])!
//                    if bedTime[0].endAMPM == "pm" {
//
//                        endHour += 12
//                    }
                    //AM
                    
                    let event = EKEvent(eventStore: self.eventStore)
                    event.title = classesListForEvents[i].name!
                     event.alarms = []
                    
                   event.notes = classesListForEvents[i].notes!
                    
                    event.startDate = endingDate
                    event.endDate = Date(timeIntervalSinceReferenceDate: refrence + timeInSeconds)
                  //  event.calendar = self.eventStore.defaultCalendarForNewEvents
                    event.calendar = userCalendar
                    eventsToDelete.append(EventToDelete(name: event.title!))
                    saveEventToDelete()
                    //NEW THINGS --
                    startDateOfThingsAfterSchool.insert(event.startDate, at: 0)
                    endDateOfThingsAfterSchool.insert(event.endDate, at: 0)
                    //--
                    arrayOfClassesTooMuch.append(event.title)
                          
                    let numb = Double(classesListForEvents[i].time!)! - time
                    classesListForEvents[i].time! = String(numb)
                   if notiEnabled.enabled {
                            let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                                let dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: event.startDate)
                        let dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: event.endDate)
                    let center = UNUserNotificationCenter.current()
                        var startEventHour = dateComponents.hour
                                            var ampmStart = "am"
                                if startEventHour ?? 0 >= 12 {
                                            startEventHour! -= 12
                                            ampmStart = "pm"
                                                }
                                    var ampmEnd = "am"
                                    var endEventHour = dateComponents2.hour
                                        if endEventHour ?? 0 >= 12 {
                                            endEventHour! -= 12
                                                ampmEnd = "pm"
                                                            }
                                              let content = UNMutableNotificationContent()
                    var startEventMinute = String(dateComponents.minute!)
                                           if dateComponents.minute! < 10 {
                                               startEventMinute = "0\(dateComponents.minute!)"
                                           }
                                           var endEventMinute = String(dateComponents2.minute!)
                                           if dateComponents2.minute! < 10 {
                                               endEventMinute = "0\(dateComponents2.minute!)"
                                           }
                                           content.title = "\(event.title!) for \(Int(time)) minutes \(startEventHour ?? 0):\(startEventMinute) \(ampmStart) - \(endEventHour ?? 0):\(endEventMinute) \(ampmEnd)"
                                        
                    content.body = "Notes: \(event.notes ?? "Error")"
                                              content.categoryIdentifier = "alarm"
                                             // content.userInfo = ["customData": "fizzbuzz"]
                                              content.sound = UNNotificationSound.default
                                          
                   //
                                        var tempArray = classesList
                                                               tempArray.sort(by: { $0.priority! > $1.priority! })
                                                                var index2 = tempArray.firstIndex(where: { $0.name == classesListForEvents[i].name })
                                                               if index2! > 0 {
                                                               index2! -= 1
                                                               }
                                                                 
                                                               content.userInfo = [
                                                                   "nameOfClass": classesListForEvents[i].name!,
                                                                   "indexBefore": index2!,
                                                                   "classToLittle": "no",
                                                                "arrayClassesTooMuch": arrayOfClassesTooMuch
                                                               ]
                                           
                                           
                                           let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
                                           let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
                                           center.add(request)
                                       }
                    do {
                        try self.eventStore.save(event, span: .thisEvent)
                        
                        
                        
                    } catch let error {
                        ac(title: "Error", message: "Class Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")
                        
                    }
                    
                    
                    
                }
            doneWithFunction = true
        } else {
            
            plantedSuccessfully = false
            doneWithFunction = true
        }
        } else {
            //isClass == false
            let time = freeTimeTime
            let timeInSeconds = time * 60
            if time > 0 {
            //    let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
            
            plantedSuccessfully = true
            
            let refrence = endingDate.timeIntervalSinceReferenceDate
            
            
            let event = EKEvent(eventStore: self.eventStore)
            event.title = "Chunked Free Time"
             event.alarms = []

            //was starting date
            event.startDate = endingDate
            event.endDate = Date(timeIntervalSinceReferenceDate: refrence + Double(timeInSeconds))
            //NEW THINGS --
            startDateOfThingsAfterSchool.insert(event.startDate, at: 0)
            endDateOfThingsAfterSchool.insert(event.endDate, at: 0)
            //--
         //   event.calendar = self.eventStore.defaultCalendarForNewEvents
              event.calendar = userCalendar
            eventsToDelete.append(EventToDelete(name: event.title!))
            saveEventToDelete()
          //  classesListForEvents.remove(at: i)
            do {
                try self.eventStore.save(event, span: .thisEvent)
                
                
                
            } catch let error {
                ac(title: "Error", message: "Chunked Free Time Event failed with error \(error.localizedDescription). If you can, try to fix the problem, otherwise email us.", button: "OK")
                
            }
            
            
            
            
            }
            
        }
    }
    func generateCalendarToWrite() {
      
        let userCalendarInput = userCalendarArray[0].userCalTitle
        
          let calendars = eventStore.calendars(for: .event)
        for cal in calendars {
            if cal.title == userCalendarInput {
                userCalendar = cal
                print("Found Calendar")
                return
            }
        }
        ac(title: "Error", message: "Could Not Find Calendar '\(userCalendarInput)'", button: "OK")
        
    }
    func generateDateTomorrow() {
        let calendar = Calendar.current
        
        let date = Date()
        var day = calendar.component(.day, from: date)
        var month = calendar.component(.month, from: date)
        var year = calendar.component(.year, from: date)
        switch month {
          case 1:
            if day == 31 {
                month += 1
                day = 1
            } else {
                day += 1
            }
        case 2:
            if day == 28 {
                month += 1
                day = 1
            } else {
                day += 1
            }
        case 3:
            if day == 31 {
                month += 1
                day = 1
            } else {
                day += 1
            }
        case 4:
            if day == 30 {
                month += 1
                day = 1
            } else {
                day += 1
            }
        case 5:
            if day == 31 {
                month += 1
                day = 1
            } else {
                day += 1
            }
        case 6:
            if day == 30 {
                month += 1
                day = 1
            } else {
                day += 1
            }
        case 7:
            if day == 31 {
                month += 1
                day = 1
            } else {
                day += 1
            }
        case 8:
            if day == 31 {
                month += 1
                day = 1
            } else {
                day += 1
            }
        case 9:
        if day == 30 {
            month += 1
            day = 1
        } else {
            day += 1
            }
        
        case 10:
        if day == 31 {
            month += 1
            day = 1
        } else {
            day += 1
        }
        
        case 11:
            if day == 30 {
                month += 1
                day = 1
            } else {
                day += 1
            }
        case 12:
            if day == 31 {
                year += 1
                month = 1
                day = 1
            } else {
                day += 1
            }
        default:
            ac(title: "Error", message: "Month is not in switch statment. Please Email Us.", button: "OK")
        }
        dayTomorrow = day
        monthTommorrow = month
        yearTommorrow = year
        
    }
    
    func generateDateToday() {
        let calendar = Calendar.current
        
        let date = Date()
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        dayToday = day
        monthToday = month
       yearToday = year
        
    }
    
    func saveEventToDelete() {
        let jsonEncoder = JSONEncoder()
        if let saveData = try? jsonEncoder.encode(eventsToDelete) {
            let defaults = UserDefaults.standard
            
            defaults.set(saveData, forKey: "SYNCevents")
        } else {
            print("Failed TO load")
        }

    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // pull out the buried userInfo dictionary
        var dyn: Bool = false
        let defaults = UserDefaults.standard
                             if let savedSet = defaults.object(forKey: "SYNCdynSchedule") as? Data {
                                           do {
                                               let jsonDecoder = JSONDecoder()
                                            var test = try jsonDecoder.decode(saveDynScheduling.self, from: savedSet)
                                            dyn = test.enabled
                                           } catch {
                                            dyn = false
                                               print("Failed To load SYNCdynSchedule")
                                           }
                             } else {
                                dyn = false
        }
        if !dyn { return }
        self.classesListForEvents = classesList
        self.classesListForEvents.sort(by: { $0.priority! > $1.priority! })
        let userInfo = response.notification.request.content.userInfo
        print("acted on response")
        if let name = userInfo["nameOfClass"] as? String? {
           
         
                if name == classesListForEvents[0].name {
                    
                    
                return
                    
            }
            
        }
        let index = userInfo["indexBefore"] as? Int
       
            
        
        let ac = UIAlertController(title: "Did you finish the previous homework? Are you ready to move on with the schedule?", message: "(Dynamic Scheduling)", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Yes", style: .cancel, handler: nil))
        let noButton = UIAlertAction(title: "No", style: .default) {
            [weak self, weak ac] action in
            self?.makeViaNoti = false
            
           var deleteCounter = 0
            for clas in self!.classesListForEvents {
                if (self?.classesListForEvents.firstIndex(where: { $0.name == clas.name})!)! < (self?.classesListForEvents.firstIndex(where: { $0.name == self?.classesListForEvents[index!].name!})!)! {
                    print("\(self?.classesListForEvents[index!].name) is higher than \(clas.name)")
                    deleteCounter += 1
                   
                }
                //we could do if less priorty then erase it but then what if 2 things have same pirorty??
            }
            
            for _ in 0..<deleteCounter {
                print("CUT CLASS")
                self?.classesListForEvents.remove(at: 0)
            }
            let newAC = UIAlertController(title: "New Time (Minutes)", message: "For Previous Homework so you can continue to work on it (\(self?.classesListForEvents[0].name! ?? "ERROR")) Note - You will lose the same amount of minutes to chunked free time", preferredStyle: .alert)
            newAC.addTextField(configurationHandler: { textField in
                                      textField.keyboardType = .numberPad
                                  })
            let submit = UIAlertAction(title: "Enter", style: .default) {
                [weak self, weak newAC] action in
                
                guard let answer = newAC?.textFields?[0].text else { self?.ac(title: "Error Dynamic Scheduling Stopped", message: "Please enter a minutes next time", button: "OK");  return }
                if relaxTime.count > 0 {
                    self?.minutesOfChunkedRelaxTime += relaxTime[0].time
                       self?.minutesOfChunkedRelaxTime -= Int(answer)!
                    if self!.minutesOfChunkedRelaxTime < 0 {
                        self?.minutesOfChunkedRelaxTime = 0
                    }
                       }
            
             
                
                self?.classesListForEvents[0].time = answer
               self?.FTPO = true
                          self?.generateAfteroon = true
                          self?.makeViaNoti = true
                        self?.createSchedule()
                
            }
            newAC.addAction(submit)
            self?.present(newAC, animated: true)
            
           
            //Ask how much more time they need. Make classeslistsFORNOTI save or something have write the time to that array. Then generate schedule with FTPO, and if they answer YES delete that from classesListFORNOTI
            
        }
        ac.addAction(noButton)
        present(ac, animated: true)
//        if let customData = userInfo["indexOfClass"] as? Int {
//            print("Custom data received: \(customData)")
//            //perhaps no matter how the open it ask if they want to remake schedule?
//
//            switch response.actionIdentifier {
//            case UNNotificationDefaultActionIdentifier:
//                // the user swiped to unlock
//                print("Default identifier")
//
//            case "show":
//                // the user tapped our "show more info…" button
//                print("Show more information…")
//
//            default:
//                break
//            }
//        }

        // you must call the completion handler when you're done
        completionHandler()
    }
    
    func ac(title: String, message: String, button: String) {
        DispatchQueue.main.async {
            
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
            self.present(alert, animated: true)
        
        }}
    
    
    //Mark: navigation functions
    
    @objc func ShowClasses()
    {
        performSegue(withIdentifier: "ShowClasses", sender: nil )
    }
    @objc func ShowSchoolHours()
    {
        performSegue(withIdentifier: "ShowSchoolHours", sender: nil )
    }
    @objc func ShowStudyHalls()
    {
        performSegue(withIdentifier: "ShowStudyHalls", sender: nil )
    }
    @objc func ShowExtraActv()
    {
        performSegue(withIdentifier: "ShowExtraActv", sender: nil )
    }
    @objc func ShowRelaxTime() {
         performSegue(withIdentifier: "ShowRelaxTime", sender: nil )
    }
    @objc func ShowBedTime() {
        performSegue(withIdentifier: "ShowBedTime", sender: nil )
    }
    @objc func ShowHelp() {
         performSegue(withIdentifier: "ShowHelp", sender: nil )
    }
    @objc func ShowContact() {
         performSegue(withIdentifier: "ShowContact", sender: nil )
    }
    @objc func ShowInfo() {
            performSegue(withIdentifier: "ShowInfo", sender: nil )
       }
    @IBAction func onMoreTapped()
    {
        sideShowing = true
        print("TOGGLE SIDE MENU" )
        NotificationCenter.default.post(name: Notification.Name("reload"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil )
    }
  
    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        if sideShowing == true {
            sideShowing = false
            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil )
        }
    }
    
    @IBAction func swipeR(_ sender: UISwipeGestureRecognizer) {
        if sideShowing == true {
            sideShowing = false
            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil )
        }
    }
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        if sideShowing == false {
            sideShowing = true
            NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil )
        }
    }
   
    
    func saveUserCalendar(title: String) {
        self.eventStore.requestAccess(to: EKEntityType.event, completion:
                  {(granted, error) in
                      if !granted {
                          DispatchQueue.main.async {
                              
                          
                          self.ac(title: "Error", message: "We do not have access to your calendar. Please enable it in settings. More information can be found in our Privacy Policy.", button: "OK")
                          }
                          return
                      }
        })
        userCalendarArray.removeAll()
        userCalendarArray.append(userCal(userCalTitle: title))
        let jsonEncoder = JSONEncoder()
        if let saveData = try? jsonEncoder.encode(userCalendarArray) {
            let defaults = UserDefaults.standard
            defaults.set(saveData, forKey: "SYNCuserCal")
        } else {
            print("Failed TO Save UserCalendar")
        }
        checkChecks()
    }
    func checkDoubleActivites() -> Bool {
        let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
               
               
               
               var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
               if generateAfteroon == false {
                   dateComponents.year = self.yearTommorrow
                   dateComponents.month = self.monthTommorrow
                   dateComponents.day = self.dayTomorrow
               } else {
                   dateComponents.year = self.yearToday
                   dateComponents.month = self.monthToday
                   dateComponents.day = self.dayToday
               }
               dateComponents.hour = 0
               dateComponents.minute = 0
               dateComponents.second = 0 //second stays the same
               
               var dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
               
               if generateAfteroon == false {
                   dateComponents2.year = self.yearTommorrow
                   dateComponents2.month = self.monthTommorrow
                   dateComponents2.day = self.dayTomorrow
               } else {
                   dateComponents2.year = self.yearToday
                   dateComponents2.month = self.monthToday
                   dateComponents2.day = self.dayToday
               }
               dateComponents2.hour = 24
               dateComponents2.minute = 59
               dateComponents2.second = 0 //Second stays the same
               var startDate = Date()
        var endDate = Date()
        if generateAfteroon {
            startDate = schoolHourEndDate.changeDateToDateToday()
            endDate = startOfBedTime.changeDateToDateToday()
        } else {
            startDate = schoolHourEndDate.changeDateToDateTommorow()
                       endDate = startOfBedTime.changeDateToDateTommorow()
        }
             
        
        let predicate2 = eventStore.predicateForEvents(withStart: Calendar.current.date(from: dateComponents)!, end:  Calendar.current.date(from: dateComponents2)!, calendars: nil)
               
               
               let eV = eventStore.events(matching: predicate2) as [EKEvent]
               //1. Reading and giving variables to times
                  
        for thing in eV {
            if thing.calendar != userCalendar { continue }
            
                   for i in eV {
                    if i.calendar != userCalendar { continue }
                    if thing == i {
                         //TO CHECK IF THEY ARE SAME
                        continue
                    }
                    if thing.startDate == i .endDate {
                        continue
                    }
//                    if thing.startDate == i.startDate || thing.startDate == i.endDate {
//                                          continue
//                                      }
                    if thing.startDate.isBetween(i.startDate, and: i.endDate) {
                        doubleActivites = true
                        doubleActivitesRangeTitle = i.title
                        doubleActivitesBetweenTitle = thing.title
                        return true
                    }
                      
        }
        }
        return false
    }
   
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
          return 1
      }
      
      func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listOfUserCalendars.count
      }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listOfUserCalendars[row]
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
   
  
    
    @IBAction func generateButton(_ sender: UIButton) {
        switch segWeak.selectedSegmentIndex {
        case 0:
            if !schoolHours.isEmpty && !bedTime.isEmpty {
                   if Date() > schoolHours[0].endTime.changeDateToDateToday() {
                         FTPO = true
                   } else {
                       FTPO = false
                   }
                   }
                       makeViaNoti = false
                   generateAfteroon = true
                   createSchedule()
        case 1:
            generateAfteroon = true
                   FTPO = false
                     makeViaNoti = false
                   createSchedule()
            
        case 2:
            generateAfteroon = false
                   FTPO = false
                   makeViaNoti = false
                   createSchedule()
                   
        default:
            ac(title: "ERROR", message: "Starting point cannot be processed", button: "OK")
        }
    }
    @IBOutlet var segWeak: UISegmentedControl!
    
    
    
    
    
    
    
    
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-6695085642587274/4790252867")
      interstitial.delegate = self
      interstitial.load(GADRequest())
      return interstitial
    }

    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
     doneWithGen()
    }
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
      print("interstitialDidReceiveAd")
    }
    func doneWithGen() {
        interstitial = createAndLoadInterstitial()
               if iCloudSync {
               MKiCloudSync.start(withPrefix: "SYNC")
                
               }
              if alertLine1 == "" {
                       alertLine1 = "No Alerts"
                       alertLine3 = "-"
                       alertLine2 = "-"
                       alertLineTest = "-"
                   }
                   let ac = UIAlertController(title: "Alerts", message: "\(alertLine1) \n \(alertLine2) \n \(alertLine3)", preferredStyle: .alert)
                   ac.addAction(UIAlertAction(title: "Open Calendar", style: .default) {
                       action in
                       let instagramHooks = "calshow://"
                       let instagramUrl = NSURL(string: instagramHooks)
                       
                       UIApplication.shared.open(instagramUrl! as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                   })
                   ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                 present(ac, animated: true)
                   alertLine1 = ""
                   alertLine3 = ""
                   alertLine2 = ""
    }
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
      print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
        doneWithGen()
    }

    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
      print("interstitialWillPresentScreen")
    }

    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
      print("interstitialWillDismissScreen")
    }

    /// Tells the delegate the interstitial had been animated off the screen.
    

    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
      print("interstitialWillLeaveApplication")
    }
    @IBAction func calendarEdit(_ sender: UIButton) {
         let calendars = eventStore.calendars(for: .event)
                listOfUserCalendars.removeAll()
                       for cal in calendars {
                        if cal.allowsContentModifications {
                        listOfUserCalendars.append(cal.title)
                        }
                       }
                let ac = UIAlertController(title: "Pick Calendar", message: "Note - To generate a schedule we will delete all events from the Calendar for the day. \n\n\n\n\n\n\n", preferredStyle: .alert)
                
                //  Create a frame (placeholder/wrapper) for the picker and then create the picker
                let pickerFrame = CGRect(x: 0, y: 50, width: 270, height: 150) // CGRectMake(left), top, width, height) - left and top are like margins
                let picker = UIPickerView(frame: pickerFrame)

                //  set the pickers datasource and delegatez
                picker.delegate = self
                picker.dataSource = self

                //  Add the picker to the alert controller
                ac.view.addSubview(picker)
                
                let submitAction =  UIAlertAction(title: "Enter", style: .default) {
                           [weak self] action in
                        
                    let numb = picker.selectedRow(inComponent: 0)
                    self?.saveUserCalendar(title: (self?.listOfUserCalendars[numb])!)
                    self?.calendarButtonText.setTitle("Calendar To Write To: \(self?.listOfUserCalendars[numb] ?? "Error")", for: .normal)
                }
                
               
                
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                ac.addAction(submitAction)
                
                present(ac, animated: true)
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
