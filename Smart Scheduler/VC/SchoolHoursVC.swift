//
//  SchoolHoursVC.swift
//  Smart Scheduler
//
//  Created by Ankit R Kumar on 7/6/18.
//  Copyright © 2018 CompuBaba. All rights reserved.
//

import UIKit
var schoolHours = [SchoolHour]()

class SchoolHoursVC: UIViewController {
    var invalid = false
   
   
    @IBOutlet var startPicker: UIDatePicker!
    @IBOutlet var endPicker: UIDatePicker!
    
    @IBOutlet var hourLabel: UILabel!
    // var db: OpaquePointer?
   override func viewWillAppear(_ animated: Bool)
        
    {
        hourLabel.adjustsFontSizeToFitWidth = true
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTotalView"), object: nil)

        if #available(iOS 13.0, *) {
        
            if traitCollection.userInterfaceStyle == .dark {
             
                view.backgroundColor = UIColor.black
           
           
            } else {
                view.backgroundColor = UIColor.white
            
            }
        } else {
            view.backgroundColor = UIColor.white
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.title = "School Hours"
        hourLabel.adjustsFontSizeToFitWidth = true
    }
    

   
    
    func ac(title: String, message: String, button: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
        present(alert, animated: true)
        
    }
    
    @IBAction func enter(_ sender: UIButton)
    {
        doneAction()
        
        
    }
    func doneAction() {
       
        schoolHours.removeAll()
        
        
        schoolHours.append(SchoolHour(id: 0, startTime: startPicker.date, endTime: endPicker.date))
     
        
        print(startPicker.date)
        reloadLabel()
        save()
        
        print("Data saved successfully")
    }
    func checkValid(_ check: String?) {
        var makeReturn = false
        if !(check?.contains(":"))! || !(check?.contains(":"))!  {
            makeReturn = true
        } else {
            let seperated = check?.split(separator: ":", maxSplits: 2, omittingEmptySubsequences: false)
            
            if Int(seperated![0]) ?? 100 > 12 || Int(seperated![1]) ?? 100 > 60 {
                
                makeReturn = true
            }
            if seperated![0].count != 2 || seperated![1].count != 2 {
                makeReturn = true
            }
        }
        if check?.count ?? 100 > 5 {
            makeReturn = true
        }
        
        
      
        if makeReturn == true {
            makeReturn = false
            ac(title: "Invalid", message: "Invalid Time please Answer in format XX:XX (Examples: 08:35, 11:35)", button: "OK")
            invalid = true
        }
    }
    
   
    
    func save() {
       
        
        let jsonEncoder = JSONEncoder()
        if let saveData = try? jsonEncoder.encode(schoolHours) {
            let defaults = UserDefaults.standard
            defaults.set(saveData, forKey: "SYNCschoolHours")
        } else {
            print("Failed TO load")
        }
        
        
      
    
        
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
      
        let defaults = UserDefaults.standard
        if let savedClass = defaults.object(forKey: "SYNCschoolHours") as? Data {
            let jsonDecoder = JSONDecoder()
            do {
                schoolHours = try jsonDecoder.decode([SchoolHour].self, from: savedClass)
                if !schoolHours.isEmpty {
                startPicker.date = schoolHours[0].startTime
                endPicker.date = schoolHours[0].endTime
                
                endPicker.minimumDate = startPicker.date
                }
                reloadLabel()

            } catch {
                print("Failed TO load")
            }
        } else {
              let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
            var dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
                   dateComponents.hour = 7
                   dateComponents.minute = 0
                    var dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: Date())
                   dateComponents2.hour = 15
                   dateComponents2.minute = 0
            
            startPicker.date = Calendar.current.date(from: dateComponents)!
            endPicker.date = Calendar.current.date(from: dateComponents2)!
        }
      if let savedBed = defaults.object(forKey: "SYNCbedTime") as? Data {
                let jsonDecoder = JSONDecoder()
                do {
                    bedTime = try jsonDecoder.decode([BedTime].self, from: savedBed)
                    if !bedTime.isEmpty {
                        startPicker.minimumDate = bedTime[0].endTime.changeDateToDateToday()
                        endPicker.maximumDate = bedTime[0].startTime.changeDateToDateToday()
                    }
                } catch {
                    print("Failed TO load")
                }
            }
    
        let documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last! as String
        print(documentPath)
      
    }
    
   
    func reloadLabel() {
        if !schoolHours.isEmpty {
              let calendarUnitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                  
            let dateComponents = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: schoolHours[0].startTime)
                    
           var Shour = dateComponents.hour!
            var Sminute = String(dateComponents.minute!)
            
            let dateComponents2 = (Calendar.current as NSCalendar).components(calendarUnitFlags, from: schoolHours[0].endTime)
            var EHour = dateComponents2.hour!
            var EMinute = String(dateComponents2.minute!)
             if Int(EMinute)! < 10 {
                                      EMinute = "0\(EMinute)"
                                  }
                                  if Int(Sminute)! < 10 {
                                      Sminute = "0\(Sminute)"
                                  }
           let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
            if dateFormat.firstIndex(of: "a") == nil {
               hourLabel.text = "\(Shour):\(Sminute) - \(EHour):\(EMinute)"
            } else {
                
                     
                  
                         var startAMPM = ""
                          var endAMPM = ""
                          if Shour == 12 {
                              startAMPM = "pm"
                          } else {
                              if Shour <= 12 {
                                                 startAMPM = "am"
                                             } else {
                                                 Shour -= 12
                                                 startAMPM = "pm"
                                             }
                          }
                          if EHour == 12 {
                              endAMPM = "pm"
                          } else {
                              if EHour <= 12 {
                                          endAMPM = "am"
                                            } else {
                                                EHour -= 12
                                                endAMPM = "pm"
                                            }
                                            
                          }
                         
                
                hourLabel.text = "\(Shour):\(Sminute) \(startAMPM) - \(EHour):\(EMinute) \(endAMPM)"
            }
          
          
        }
    }
   
    @IBAction func startTimeAction(_ sender: UIDatePicker) {
        endPicker.minimumDate = startPicker.date
    }
    
    
   
    
}
