//
//  SideMenuVC.swift
//  Smart Scheduler
//
//  Created by Ankit R Kumar on 7/1/18.
//  Copyright © 2018 CompuBaba. All rights reserved.
//

import UIKit
import MessageUI
class SideMenuVC: UITableViewController, MFMailComposeViewControllerDelegate
{
    var makeReturn = false
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableData), name: NSNotification.Name(rawValue: "reload"), object: nil)
       
       
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        print("cellForRowAt")
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0: cell.textLabel?.text = "Add Homework"
              case 1:
                if !stopAccessToEverythingButBedTime && stopAccessToEverythingButSchoolHours {
                    cell.backgroundColor = .red
                }
                cell.textLabel?.text = "Set School Hours"
             // case 2: cell.textLabel?.text = "Add Study Halls"
              case 2: cell.textLabel?.text = "Add Extracurricular Activities"
              cell.textLabel?.adjustsFontSizeToFitWidth = true
              case 3: cell.textLabel?.text = "Add Free Time"
            case 4: if stopAccessToEverythingButBedTime {
               
                     cell.backgroundColor = .red
                    
                
               
            }
                 cell.textLabel?.text = "Set Bed Time"
                 
            default:
                cell.textLabel?.text = "ERROR"
                
            }
        } else {
            if indexPath.row == 0 {
                cell.textLabel?.text = "Info"
            } else {
                if indexPath.row == 1 {
                    cell.textLabel?.text = "Contact"
                }
            }
        }
        return cell
    }
    @objc func reloadTableData(_ notification: Notification) {
        tableView.reloadData()
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print(indexPath.row)
       
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil )
        
        switch indexPath.row
        {
            case 0:
                if indexPath.section == 0 {
                  checkAccess()
                      if makeReturn { return }
                    makeReturn = false
                NotificationCenter.default.post(name: NSNotification.Name("ShowClasses"), object: nil)
                } else if indexPath.section == 1 {
                     NotificationCenter.default.post(name: NSNotification.Name("ShowInfo"), object: nil)
                    
            }
            case 1:
                if indexPath.section == 0 {
                    if stopAccessToEverythingButBedTime {
                        ac(title: "Problem", message: "Looks like you're a new user. Please give times for Bed Time First", button: "OK")
                            sideShowing = false
                        return
                    }
                     makeReturn = false
                NotificationCenter.default.post(name: NSNotification.Name("ShowSchoolHours"), object: nil)
                } else if indexPath.section == 1 {
                   //NotificationCenter.default.post(name: NSNotification.Name("ShowContact"), object: nil)
                    if MFMailComposeViewController.canSendMail() {
                           let mail = MFMailComposeViewController()
                           mail.mailComposeDelegate = self
                           mail.setToRecipients(["smartscheduleradmob@gmail.com"])
                        mail.setSubject("Smart Scheduler - Student")

                           present(mail, animated: true)
                       } else {
                          ac(title: "Error", message: "Device Can't Send Mail", button: "OK")
                       }
            }
//            case 2:
//                if indexPath.section == 0 {
//                    checkAccess()
//                      if makeReturn { return }
//                      makeReturn = false
//                NotificationCenter.default.post(name: NSNotification.Name("ShowStudyHalls"), object: nil)
//                } else if indexPath.section == 1 {
//
//            }
            case 2:
                checkAccess()
                  if makeReturn { return }
                  makeReturn = false
                NotificationCenter.default.post(name: NSNotification.Name("ShowExtraActv"), object: nil)
            
            case 3:
                checkAccess()
                if makeReturn { return }
                  makeReturn = false
                NotificationCenter.default.post(name: NSNotification.Name("ShowRelaxTime"), object: nil)
            case 4:
                 makeReturn = false
                NotificationCenter.default.post(name: NSNotification.Name("ShowBedTime"), object: nil)
           
            default: break
        }
        tableView.deselectRow(at: indexPath, animated: false)
        sideShowing = false
         tableView.reloadData()
    }
    
    func checkAccess() {
        if stopAccessToEverythingButSchoolHours {
            if stopAccessToEverythingButBedTime {
                ac(title: "Problem", message: "Looks like you're a new user. Please give times for Bed Time First", button: "OK")
                makeReturn = true
                 sideShowing = false
            } else {
                ac(title: "Problem", message: "Please give times for School Hours", button: "OK")
                makeReturn = true
                sideShowing = false
            }
        }
    }
    func ac(title: String, message: String, button: String) {
          DispatchQueue.main.async {
              
          
          let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: button, style: .default, handler: nil))
              self.present(alert, animated: true)
          
          }}
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
      
}
