//
//  BedTimeTableViewCell.swift
//  Smart Scheduler
//
//  Created by Aayush R Kumar on 7/29/19.
//  Copyright © 2019 CompuBaba. All rights reserved.
//

import UIKit

class BedTimeTableViewCell: UITableViewCell {

    @IBOutlet var endLabel: UILabel!
    @IBOutlet var startLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
