//
//  ClassesTableViewCell.swift
//  Smart Scheduler
//
//  Created by Aayush R Kumar on 7/17/19.
//  Copyright © 2019 CompuBaba. All rights reserved.
//

import UIKit

class ClassesTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        classLabel.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    @IBOutlet var timeLabel: UILabel!
    
    @IBOutlet var priortyLabel: UILabel!
    
    @IBOutlet var classLabel: UILabel!
}
