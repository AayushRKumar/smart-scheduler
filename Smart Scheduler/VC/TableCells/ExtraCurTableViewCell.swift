//
//  ExtraCurTableViewCell.swift
//  Smart Scheduler
//
//  Created by Aayush R Kumar on 7/18/19.
//  Copyright © 2019 CompuBaba. All rights reserved.
//

import UIKit

class ExtraCurTableViewCell: UITableViewCell {

    @IBOutlet var startLabel: UILabel!
    @IBOutlet var endLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        nameLabel.adjustsFontSizeToFitWidth = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
