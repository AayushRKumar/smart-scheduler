//
//  MainTableViewCell.swift
//  Smart Scheduler
//
//  Created by Aayush R Kumar on 1/4/20.
//  Copyright © 2020 CompuBaba. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
      var buttonAction: ((Any) -> Void)?
    var switchAction: ((Any) -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    @IBOutlet var infoWeak: UIButton!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
          
        // Configure the view for the selected state
    }

    @IBAction func infoClick(_ sender: UIButton) {
        self.buttonAction?(sender)
    }
    
    
    @IBAction func switchChange(_ sender: UISwitch) {
        self.switchAction?(sender)
    }
    
  
    @IBOutlet var switchButton: UISwitch!
    
    @IBOutlet var label: UILabel!
    
}
