//
//  SchoolHourTableViewCell.swift
//  Smart Scheduler
//
//  Created by Aayush R Kumar on 7/17/19.
//  Copyright © 2019 CompuBaba. All rights reserved.
//

import UIKit

class SchoolHourTableViewCell: UITableViewCell {

    @IBOutlet var startLabel: UILabel!
    @IBOutlet var endLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
