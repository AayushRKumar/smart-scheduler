//
//  infoViewController.swift
//  Smart Scheduler
//
//  Created by Aayush R Kumar on 1/26/20.
//  Copyright © 2020 CompuBaba. All rights reserved.
//

import UIKit

class infoViewController: UIViewController {

    @IBOutlet var vLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        vLabel.text = "Version: \(appVersionString)"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func policy(_ sender: UIButton) {
        guard let url = URL(string: "https://smart-scheduler-student.weebly.com/privacy-policy.html") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func help(_ sender: UIButton) {
        guard let url = URL(string: "https://smart-scheduler-student.weebly.com") else { return }
        UIApplication.shared.open(url)
    }
}
